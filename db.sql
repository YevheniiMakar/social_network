-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: social_network
-- ------------------------------------------------------
-- Server version	5.7.21-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `users_list_id` int(11) NOT NULL,
  `status` enum('FRIEND','SUBSCRIBER','IGNOR') NOT NULL DEFAULT 'SUBSCRIBER',
  PRIMARY KEY (`id`),
  KEY `fk_contacts_users_idx` (`users_id`),
  CONSTRAINT `fk_contacts_users` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
INSERT INTO `contacts` VALUES (1,2,3,'SUBSCRIBER');
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medias`
--

DROP TABLE IF EXISTS `medias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `data_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `media_type` enum('VIDEO','AUDIO','PICTURE') NOT NULL,
  `status` enum('ACTIVE','DELETED','BAN') NOT NULL DEFAULT 'ACTIVE',
  PRIMARY KEY (`id`),
  KEY `fk_medias_users1_idx` (`users_id`),
  CONSTRAINT `fk_medias_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medias`
--

LOCK TABLES `medias` WRITE;
/*!40000 ALTER TABLE `medias` DISABLE KEYS */;
INSERT INTO `medias` VALUES (2,2,'default-user-image.png','2018-02-20 14:04:57','PICTURE','ACTIVE'),(3,2,'161b73531b5161b73531b5161b73531b5.mp3','2018-02-21 09:12:52','AUDIO','ACTIVE'),(4,2,'161bd54dd14161bd54dd14161bd54dd14.jpg','2018-02-22 13:45:11','PICTURE','ACTIVE'),(5,2,'161bd55158f161bd55158f161bd55158f.mp3','2018-02-22 13:45:25','AUDIO','ACTIVE'),(6,2,'161c9d01af3161c9d01af3161c9d01af3.jpg','2018-02-24 23:55:14','PICTURE','ACTIVE'),(7,2,'161c9d095a6161c9d095a6161c9d095a6.png','2018-02-24 23:55:46','PICTURE','ACTIVE'),(8,2,'161c9d0f168161c9d0f168161c9d0f168.jpg','2018-02-24 23:56:09','PICTURE','ACTIVE'),(9,2,'161c9d6ba8f161c9d6ba8f161c9d6ba8f.jpg','2018-02-25 00:02:28','PICTURE','ACTIVE'),(10,2,'161c9d6dc9b161c9d6dc9b161c9d6dc9b.png','2018-02-25 00:02:37','PICTURE','ACTIVE'),(11,2,'161c9d6fa2b161c9d6fa2b161c9d6fa2b.jpg','2018-02-25 00:02:45','PICTURE','ACTIVE'),(12,2,'161c9d73a33161c9d73a33161c9d73a33.png','2018-02-25 00:03:01','PICTURE','ACTIVE'),(13,2,'161c9d7543f161c9d7543f161c9d7543f.jpg','2018-02-25 00:03:08','PICTURE','ACTIVE'),(14,2,'161c9d78849161c9d78849161c9d78849.png','2018-02-25 00:03:21','PICTURE','ACTIVE'),(15,3,'default-user-image.png','2018-02-20 14:04:57','PICTURE','ACTIVE'),(16,4,'default-user-image.png','2018-02-20 14:04:57','PICTURE','ACTIVE');
/*!40000 ALTER TABLE `medias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `posts` varchar(500) NOT NULL,
  `data_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('ACTIVE','DELETED','BAN') NOT NULL DEFAULT 'ACTIVE',
  `visibility` enum('FRIENDS','ALL') NOT NULL DEFAULT 'ALL',
  PRIMARY KEY (`id`),
  KEY `fk_posts_users1_idx` (`users_id`),
  CONSTRAINT `fk_posts_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (11,2,'first post','2018-02-22 13:54:42','ACTIVE','ALL');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts_medias`
--

DROP TABLE IF EXISTS `posts_medias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts_medias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `posts_id` int(11) NOT NULL,
  `medias_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts_medias`
--

LOCK TABLES `posts_medias` WRITE;
/*!40000 ALTER TABLE `posts_medias` DISABLE KEYS */;
INSERT INTO `posts_medias` VALUES (5,11,4),(6,11,5);
/*!40000 ALTER TABLE `posts_medias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `regist_recover`
--

DROP TABLE IF EXISTS `regist_recover`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `regist_recover` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `firstname` varchar(45) NOT NULL,
  `lastname` varchar(45) NOT NULL,
  `hashkey` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `regist_recover`
--

LOCK TABLES `regist_recover` WRITE;
/*!40000 ALTER TABLE `regist_recover` DISABLE KEYS */;
/*!40000 ALTER TABLE `regist_recover` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(30) NOT NULL,
  `password` varchar(64) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `data_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `role` enum('ROLE_USER','ROLE_ADMIN') NOT NULL DEFAULT 'ROLE_USER',
  `status` enum('ACTIVE','DELETED','BAN') NOT NULL DEFAULT 'ACTIVE',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (2,'index1@index.com','a410ed06e41f5d3d472ef6cab49f50adb09f5580','vasya2','pupkin2','2017-10-05 19:26:11','ROLE_USER','ACTIVE'),(3,'index2@index.com','a410ed06e41f5d3d472ef6cab49f50adb09f5580','vasya','pupkin','2017-10-05 19:26:11','ROLE_USER','ACTIVE'),(4,'index3@index.com','a410ed06e41f5d3d472ef6cab49f50adb09f5580','vasya4','pupkin4','2017-10-05 19:26:11','ROLE_USER','ACTIVE');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_medias`
--

DROP TABLE IF EXISTS `users_medias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_medias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `medias_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_medias`
--

LOCK TABLES `users_medias` WRITE;
/*!40000 ALTER TABLE `users_medias` DISABLE KEYS */;
INSERT INTO `users_medias` VALUES (1,2,2),(2,3,2),(3,4,2);
/*!40000 ALTER TABLE `users_medias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wall`
--

DROP TABLE IF EXISTS `wall`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wall` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `visibility` enum('FRIENDS','ALL') NOT NULL DEFAULT 'ALL',
  `data_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_Wall_users_idx` (`users_id`),
  KEY `fk_Wall_posts1_idx` (`post_id`),
  CONSTRAINT `fk_Wall_posts11` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Wall_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wall`
--

LOCK TABLES `wall` WRITE;
/*!40000 ALTER TABLE `wall` DISABLE KEYS */;
INSERT INTO `wall` VALUES (1,2,11,'ALL','2018-02-22 13:54:42');
/*!40000 ALTER TABLE `wall` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-05  9:54:53
