/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myhomework.myfinalproject.domain.DAO;

import com.myhomework.myfinalproject.domain.BaseEntity.Medias;
import java.util.List;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author mag_o
 */
@Component
public class MediasDaoImpl extends AbstractDao <Medias, Integer> implements MediasDao{

    @Override
    @Transactional
    public List<Medias> getMedias(int offset, int limit, String type) {
        return getCurrentSession().createSQLQuery
                ("select * from medas where media_tipe = :mtype  order by date_created DESC limit :offset, :limit")
                .addEntity(Medias.class)
                .setParameter("offset", offset)
                .setParameter("limit", limit)
                .setParameter("mtype", type)
                .list();
    }

    @Override
    @Transactional
    public List<Medias> getMedias(int offset, int limit, String type, int idUsers) {
        return getCurrentSession().createSQLQuery
                ("select * from medias where media_type = :mtype and users_id = :id order by data_created DESC limit :offset, :limit")
                .addEntity(Medias.class)
                .setParameter("offset", offset)
                .setParameter("limit", limit)
                .setParameter("mtype", type)
                .setParameter("id", idUsers)
                .list();
    }
    @Override
    @Transactional
    public Medias byName(String name) {
        return (Medias)getCurrentSession().createCriteria(Medias.class)
                .add(Restrictions.eq("name", name)).uniqueResult();
    }
   /* @Override
    public Medias byId(Integer id){
        
        Medias medias = (Medias)getCurrentSession().createCriteria(Medias.class).add(Restrictions.eq("id", id)).uniqueResult();
        
        return medias;


//        return (Medias)getCurrentSession().createCriteria(Medias.class)
//                .add(Restrictions.eq("id", id)).uniqueResult();
        
    }*/
     @Override
     @Transactional
    public int getMediasCount( int id, String type ) {
        return (Integer)getCurrentSession().createSQLQuery
                               
                ("select count(*) as counter from medias where users_id = :id AND media_type = :mediatype")
//                ("select count(*) as counter from posts where (users_id = :id and visibility= 'ALL') "
//                + "or (visibility= 'FRIENDS'  users_id = ("
//                +"select users_id from contacts where users_id = :id and users_list_id = :idlu"
//                + ")) order by data_created")
                .addScalar("counter", StandardBasicTypes.INTEGER)
                .setParameter("id", id)
                .setParameter("mediatype", type)
                .uniqueResult();
    }
    
    
}
