/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myhomework.myfinalproject.domain.DAO;

import com.myhomework.myfinalproject.domain.BaseEntity.RegistRecover;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class RegistRecoverDaoImpl extends AbstractDao <RegistRecover, Integer> implements RegistRecoverDao{

    @Override
    @Transactional
    public RegistRecover byHashkey(String hashkey) {
           return (RegistRecover)getCurrentSession().createCriteria(RegistRecover.class)
                .add(Restrictions.eq("hashkey", hashkey)).uniqueResult();
    }
    
    @Override
    @Transactional
    public RegistRecover byLogin(String login){
    return (RegistRecover)getCurrentSession().createCriteria(RegistRecover.class)
                .add(Restrictions.eq("login", login)).uniqueResult();
    }

   /*@Override
    public RegistRecover byId(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }*/
    
}
