/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.myhomework.myfinalproject.domain.DAO;

import com.myhomework.myfinalproject.domain.BaseEntity.Posts;
import java.util.List;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
//import org.hibernate.Session;


@Component
public class PostsDaoImpl extends AbstractDao <Posts, Integer> implements PostsDao {

    @Override
    @Transactional
    public int getPostsCount() {
        return (Integer)getCurrentSession().createSQLQuery
                               
                ("select count(*) as counter from posts")
//                ("select count(*) as counter from posts where (users_id = :id and visibility= 'ALL') "
//                + "or (visibility= 'FRIENDS'  users_id = ("
//                +"select users_id from contacts where users_id = :id and users_list_id = :idlu"
//                + ")) order by data_created")
                .addScalar("counter", StandardBasicTypes.INTEGER)
                
                //.setParameter("idlu", idLookingUser)
                .uniqueResult();
    }
    @Override
    @Transactional
    public int getPostsCountlist( int id) {
        return (Integer)getCurrentSession().createSQLQuery
                               
                ("select count(*) as counter from posts where users_id = :id")
//                ("select count(*) as counter from posts where (users_id = :id and visibility= 'ALL') "
//                + "or (visibility= 'FRIENDS'  users_id = ("
//                +"select users_id from contacts where users_id = :id and users_list_id = :idlu"
//                + ")) order by data_created")
                .addScalar("counter", StandardBasicTypes.INTEGER)
                .setParameter("id", id)
                //.setParameter("idlu", idLookingUser)
                .uniqueResult();
    }

    @Override
    @Transactional
    public int getPostsCountWall(int id) {
        return (Integer)getCurrentSession().createSQLQuery
                ("select count(*) as counter from posts where id in(select post_id from wall where users_id =:id) ")
//                 ("select count(*) "
//                         +"from posts where id= (select post_id from wall where visibility = 'ALL' and users_id = :id and post_id =("
//	+"select id from posts where (visibility = 'ALL' and users_id = (select users_id from posts where id = ( "
//		+"select post_id from wall where users_id = :id))) or (visibility = 'FRIENDS' and users_id = ("
//			+"select users_id from contacts where users_id = ( select users_id from posts where id = ( "
//				+"select post_id from wall where users_id = :id)) and users_list_id = :idlu)))) or "
//				
//				
//				+"(select post_id from wall where visibility= 'FRIENDS' and users_id = ("
//					+"select users_id from contacts where users_id = :id and users_list_id = :idlu ) and post_id = ("
//						+"select id from posts where (visibility = 'ALL' and users_id =("
//							+"select users_id from posts where id = (select post_id from wall where users_id = :id))) or (visibility = 'FRIENDS' and users_id = ("
//								+"select users_id from contacts where users_id = ("
//									+"select users_id from posts where id = (select post_id from wall where users_id = :id)) and users_list_id = :idlu ))))"
//                         +" or (select post_id from wall where users_id=:id and users_id=:idlu )"
//                 )
                .addScalar("counter", StandardBasicTypes.INTEGER)
                .setParameter("id", id)
                //.setParameter("idlu", idLookingUser)
                .uniqueResult();
    }

    @Override
    @Transactional
    public List<Posts> getPosts(int offset, int limit) {
        return getCurrentSession().createSQLQuery
                
                ("select * from posts order by data_created DESC limit :offset, :limit ")
//                ("select * from posts where (users_id = :id and visibility= 'ALL') "
//                + "or (visibility= 'FRIENDS'  users_id = ("
//                +"select users_id from contacts where users_id = :id and users_list_id = :idlu"
//                + ")) order by data_created limit :offset, :limit")
                .addEntity(Posts.class)
                .setParameter("offset", offset)
                .setParameter("limit", limit)
                
                //.setParameter("idlu", idLookingUser)
                .list();
    }

    @Override
    @Transactional
    public List<Posts> getPosts(int offset, int limit, int id) {
        return getCurrentSession().createSQLQuery
                
                ("select * from posts where users_id = :id  order by data_created DESC limit :offset, :limit ")
                .addEntity(Posts.class)
                .setParameter("offset", offset)
                .setParameter("limit", limit)
                .setParameter("id", id)
                
                .list();
    }

    @Override
    @Transactional
    public List<Posts> getPostInWall(int offset, int limit, int id) {
       return getCurrentSession().createSQLQuery
               ("select posts.* from posts,wall where posts.id=wall.post_id and wall.users_id = :id order by posts.data_created DESC limit :offset, :limit ")
                .addEntity(Posts.class)
                .setParameter("offset", offset)
                .setParameter("limit", limit)
                .setParameter("id", id)
                //.setParameter("idlu", idLookingUser)
                .list();
    }
    @Transactional
    public List<Posts> getPostInWall(int id) {
       return getCurrentSession().createSQLQuery
               ("select posts.* from posts,wall where posts.id=wall.post_id and wall.users_id = :id order by posts.data_created DESC")
                .addEntity(Posts.class)
                .setParameter("id", id)
                //.setParameter("idlu", idLookingUser)
                .list();
    }

//    @Override
//    @Transactional
//    public Posts byId(Integer id) {
//        return (Posts)getCurrentSession().createCriteria(Posts.class)
//                .add(Restrictions.eq("id", id)).uniqueResult();
//    }

  
    
    
    
}
