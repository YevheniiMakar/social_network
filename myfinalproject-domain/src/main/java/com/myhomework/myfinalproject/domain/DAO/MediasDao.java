/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myhomework.myfinalproject.domain.DAO;

import com.myhomework.myfinalproject.domain.BaseEntity.Medias;
import java.util.List;

/**
 *
 * @author mag_o
 */
public interface MediasDao extends BaseDao<Medias, Integer>  {
    
    List<Medias> getMedias(int offset, int limit, String type);
    List<Medias> getMedias(int offset, int limit, String type, int idUsers);
    Medias byName(String name);
    Medias byId(Integer id);
    int getMediasCount( int id, String type);

    
}
