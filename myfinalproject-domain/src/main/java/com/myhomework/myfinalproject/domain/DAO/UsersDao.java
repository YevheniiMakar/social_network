
package com.myhomework.myfinalproject.domain.DAO;

import com.myhomework.myfinalproject.domain.BaseEntity.Users;
import java.util.List;

public interface UsersDao extends BaseDao<Users, Integer> {
    
    int getUsersCount();
    List<Users> getUsers(int offset, int limit);  //getAll users
    //List<Users> getUsers(int offset, int limit, int id); //get all users from frend list
//    List<Users> getUsers(int offset, int limit, int id, String status); //get all users from frend list and sort by status in frend list
//   // List<Users> getUsers(int offset, int limit, String search);// get all users for request
    //List<Users> getFriend(int offset, int limit, int id);
   //List<Users> getSubscriber(int offset, int limit, int id);
    //List<Users> getSubscription(int offset, int limit, int id);
    Users byUserName(String username);
    Users byId(Integer id);
    public List<Users> usersListWithoutFriends(int offset, int limit, int id);
    public List<Users> usersListFriends(int offset, int limit, int id);
    int usersListWithoutFriendsCount( int id);
    int usersListFriendsCount(int id);
   
    
     
    
    
}
