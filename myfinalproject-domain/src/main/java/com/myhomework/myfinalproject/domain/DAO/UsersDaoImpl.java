
package com.myhomework.myfinalproject.domain.DAO;

import com.myhomework.myfinalproject.domain.BaseEntity.Users;
//import com.sun.org.apache.xalan.internal.utils.XMLSecurityManager;
import com.sun.scenario.effect.Offset;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


@Component
public class UsersDaoImpl extends AbstractDao <Users, Integer> implements UsersDao {
    

    @Override
    @Transactional
    public int getUsersCount() {
        
        return (Integer)getCurrentSession()
                .createSQLQuery("select count(*) as counter from users")
                .addScalar("counter", StandardBasicTypes.INTEGER)
                .uniqueResult();
    }

    @Override
    @Transactional
    public List<Users> getUsers(int offset, int limit) {
        return getCurrentSession().createSQLQuery("select users.*, users_medias.medias_id from users, users_medias where users.id = users_medias.users_id")
                .addEntity(Users.class)
               /// .setParameter("offset", offset)
              //  .setParameter("limit", limit)
                .list();
    }
//    @Override
//    public List<Users> getUsers(int offset, int limit) {
//        return getCurrentSession().createSQLQuery("select * from users")
//                .addEntity(Users.class)
//               /// .setParameter("offset", offset)
//              //  .setParameter("limit", limit)
//                .list();
//    }
    
    @Override
    @Transactional
    public Users byUserName(String username) {
        return (Users)getCurrentSession().createCriteria(Users.class)
                .add(Restrictions.eq("login", username)).uniqueResult();
    }
   /* @Override
    public Users byId(Integer id){
        return (Users)getCurrentSession().createCriteria(Users.class)
                .add(Restrictions.eq("id", id)).uniqueResult();
        
    }*/
    
    @Override
    @Transactional
    public List<Users> usersListWithoutFriends(int offset, int limit, int id){
        return getCurrentSession().createSQLQuery("SELECT users.*, users_medias.medias_id "
                + "FROM users, users_medias where users.id!=:id and users_medias.users_id = users.id and users.id not in"
                + "(select contacts.users_list_id from contacts where contacts.users_id=:id) "
                + "order by users.firstname,users.lastname limit :offset, :limit ")
                .addEntity(Users.class)
                .setParameter("offset", offset)
                .setParameter("limit", limit)
                .setParameter("id", id)
                .list();
    
    }
    @Override
    @Transactional
    public List<Users> usersListFriends(int offset, int limit, int id){
        return getCurrentSession().createSQLQuery("select users.* , users_medias.medias_id from users, contacts, users_medias where " +
                 "users.id=contacts.users_list_id and contacts.users_id = :id  "
                + "and users.id!=:id and users_medias.users_id = users.id order by users.firstname,users.lastname limit :offset, :limit")
                .addEntity(Users.class)
                .setParameter("offset", offset)
                .setParameter("limit", limit)
                .setParameter("id", id)
                .list();
    
    }
    @Override
    @Transactional
    public int usersListWithoutFriendsCount( int id){
        return (Integer)getCurrentSession().createSQLQuery("SELECT COUNT(*) as counter "
                + "FROM users, users_medias where users.id!=:id and users_medias.users_id = users.id and users.id not in"
                + "(select contacts.users_list_id from contacts where contacts.users_id=:id) ")
                .addScalar("counter", StandardBasicTypes.INTEGER)
                .setParameter("id", id)
                .uniqueResult();
                
                
    
    }
    @Override
    @Transactional
    public int usersListFriendsCount( int id){
        return (Integer)getCurrentSession().createSQLQuery("select count(*) as counter from users, contacts where " +
                 "users.id=contacts.users_list_id and contacts.users_id = :id "
                + "and users.id!=:id")
                .addScalar("counter", StandardBasicTypes.INTEGER)
                .setParameter("id", id)
                .uniqueResult();
    
    }

//    @Override
//    public List<Users> getUsers(int offset, int limit, int id) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }

//    @Override
//    public List<Users> getUsers(int offset, int limit, int id, String status) {
//        List<Users> users=null;
//        if (status.equals("FRIEND")){
//            users = getCurrentSession().createSQLQuery
//        ("select * from users where id ="
//                + "(select users_id from contacts where status = 'FRIEND' "
//                + "users_id ="
//                + "(select users_list_id from contacts where users_id = :id and status = 'FRIEND' ) "
//                + ""
//                + ") order by firstname, lastname limit :offset, :limit")
//                .addEntity(Users.class)
//                .setParameter("offset", offset)
//                .setParameter("limit", limit)
//                .setParameter("id", id)
//                .list();
//        }
//        if (status.equals("SUBSCRIBER")){
//            users = getCurrentSession().createSQLQuery
//        ("select * from users where id ="
//                + "(select users_list_id from contacts where status = 'SUBSCRIBER' and "
//                + "users_id = :id"
//                + ") order by firstname, lastname limit :offset, :limit")
//                .addEntity(Users.class)
//                .setParameter("offset", offset)
//                .setParameter("limit", limit)
//                .setParameter("id", id)
//                .list();
//        
//        }
//        if (status.equals("IGNOR")){
//            users = getCurrentSession().createSQLQuery
//                ("select * from users where id ="
//                + "(select users_list_id from contacts where status = 'IGNOR' and "
//                + "users_id = :id"
//                + ") order by firstname, lastname limit :offset, :limit")
//                .addEntity(Users.class)
//                .setParameter("offset", offset)
//                .setParameter("limit", limit)
//                .setParameter("id", id)
//                .list();
//        
//        }
//        return users;
//        
//    }

//    @Override
//    public List<Users> getUsers(int offset, int limit, String search) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
/*
    @Override
    public List<Users> getFriend(int offset, int limit, int id) {
         
            return getCurrentSession().createSQLQuery
                ("select * from users where id ="
                + "(select users_list_id from contacts where users_id = :id and"
                + "users_list_id = "
                + "(select users_id from contacts where users_list_id=:id"
                + ")) order by firstname, lastname limit :offset, :limit")
                .addEntity(Users.class)
                .setParameter("offset", offset)
                .setParameter("limit", limit)
                .setParameter("id", id)
                .list();
        
    }


    @Override
    public List<Users> getSubscriber(int offset, int limit, int id) {
        return getCurrentSession().createSQLQuery
                ("select * from users where id ="
                + "(select users_id from contacts where users_list_id = :id and"
                + "users_id != "
                + "(select users_list_id from contacts where users_id=:id"
                + ")) order by firstname, lastname limit :offset, :limit")
                .addEntity(Users.class)
                .setParameter("offset", offset)
                .setParameter("limit", limit)
                .setParameter("id", id)
                .list();
    }
    @Override
    public List<Users> getSubscription(int offset, int limit, int id) {
        return getCurrentSession().createSQLQuery
                ("select * from users where id ="
                + "(select users_list_id from contacts where  id= :id and"
                + "users_list_id != "
                + "(select users_id from contacts where users_id=:id"
                + ")) order by firstname, lastname limit :offset, :limit")
                .addEntity(Users.class)
                .setParameter("offset", offset)
                .setParameter("limit", limit)
                .setParameter("id", id)
                .list();
    }
    
    */

    
}
