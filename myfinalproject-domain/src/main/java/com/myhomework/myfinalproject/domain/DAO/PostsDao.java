/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myhomework.myfinalproject.domain.DAO;

import com.myhomework.myfinalproject.domain.BaseEntity.Posts;
import java.util.List;

/**
 *
 * @author mag_o
 */
public interface PostsDao extends BaseDao<Posts, Integer> {
    int getPostsCount();
    int getPostsCountlist( int id );
    int getPostsCountWall( int id );
    List<Posts> getPosts(int offset, int limit); //get all posts
    List<Posts> getPosts(int offset, int limit, int id); //get all posts user
    List<Posts> getPostInWall(int offset, int limit, int id); //get oll posts in wall
    List<Posts> getPostInWall(int id); //get oll posts in wall
    
    
    
   
    
}
