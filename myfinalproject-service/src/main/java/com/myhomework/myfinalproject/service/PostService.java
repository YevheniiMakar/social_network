/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myhomework.myfinalproject.service;

import com.myhomework.myfinalproject.domain.BaseEntity.Posts;
import java.util.List;


public interface PostService extends BaseService<Posts, Integer>  {
    public int getPostsCount();
    public int getPostsCountlist( int id);
    public int getPostsCountWall( int id);
    public List<PostObject> getPosts(int offset, int limit); //get all posts
    public List<PostObject> getPosts(int offset, int limit, int id); //get all posts user
    public List<PostObject> getPostInWall(int offset, int limit, int id); //get oll postsst in wall
    public List<Posts> getPostInWall( int id);
}
