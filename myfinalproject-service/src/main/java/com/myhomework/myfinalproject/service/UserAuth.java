
package com.myhomework.myfinalproject.service;

import com.myhomework.myfinalproject.domain.BaseEntity.Users;


public class UserAuth extends Users{

    public UserAuth(Users user) {
        super.setId(user.getId());
        super.setFirstname(user.getFirstname());
        super.setLastname(user.getLastname());
        super.setLogin(user.getLogin());
        super.setPassword(user.getPassword());
        super.setRole(user.getRole());
        //super.setPhoto(user.getPhoto());
    }
}
