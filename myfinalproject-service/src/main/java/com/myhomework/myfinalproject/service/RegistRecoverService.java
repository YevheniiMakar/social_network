/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myhomework.myfinalproject.service;

import com.myhomework.myfinalproject.domain.BaseEntity.RegistRecover;


public interface RegistRecoverService extends BaseService<RegistRecover, Integer> {
    RegistRecover byHashkey(String hashkey);
    RegistRecover byLogin (String login);
    
}
