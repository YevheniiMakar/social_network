
package com.myhomework.myfinalproject.service;

import com.myhomework.myfinalproject.domain.BaseEntity.Users;
import java.util.List;


public interface UserService extends BaseService<Users, Integer> {
    public Users byId(Integer id);
    public UserObject getUserById(Integer id);
//    public AdminUsersObject getFullUserById(Integer id);
//    public AdminUsersListFrameObject getUsersFrame(int offset, int limit);
    Users byUserName(String userName);
    public int getUsersCount();
    public List<UserObject> getUsers(int offset, int limit);  
    //public List<Users> getFriend(int offset, int limit, int id);
    //public List<Users> getSubscriber(int offset, int limit, int id);
    //public List<Users> getSubscription(int offset, int limit, int id);
    public List<UserObject> usersListWithoutFriends(int offset, int limit, int id);
    public List<UserObject> usersListFriends(int offset, int limit, int id);
    public int usersListWithoutFriendsCount( int id);
    public int usersListFriendsCount(int id);
}