
package com.myhomework.myfinalproject.service;


//import com.myhomework.myfinalproject.domain.BaseEntity.Posts;
import com.myhomework.myfinalproject.domain.BaseEntity.Users;
import java.util.ArrayList;
//import com.myhomework.myfinalproject.domain.BaseEntity.Medias;
//import java.util.ArrayList;
import java.util.List;

public class UserObject {
    private Integer id;
    private String firstname;
    private String lastname;
    private String role;
    //private List<PostObject> postsList;
    //private List<MediasObject> mediaList;
    private List<Integer> contacts;
    private String photo;

    public UserObject() {
    }
    
    public UserObject(Users user) {
        this.id = user.getId();
        this.firstname = user.getFirstname();
        this.lastname = user.getLastname();
        this.photo = user.getPhoto().getName();
        this.role = user.getRole().toString();
        
//        this.postsList = new ArrayList<>();
//        for(Posts p : user.getPostsList()){
//            this.postsList.add(new PostObject(p));
//        }
//        
//        this.mediaList
//                = new ArrayList<>();
//        for(Medias p : user.getMediasList()){
//            this.mediaList.add(new MediasObject(p));
//        }
        this.contacts = new ArrayList<>();
        for(int i = 0; i<user.getContactsList().size(); i++){
            this.contacts.add(user.getContactsList().get(i).getId());
        }
    }
    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

//    public List<PostObject> getPosts() {
//        return postsList;
//    }
//
//    public void setPosts(List<PostObject> posts) {
//        this.postsList = posts;
//    }

//    public List<MediasObject> getMediaList() {
//        return mediaList;
//    }

//    public void setMedia(List<MediasObject> media) {
//        this.mediaList = media;
//    }
    public List<Integer> getContactsList() {
        return contacts;
    }

//    public void setContactsList(List<UsersObject> contacts) {
//        this.contacts = contacts;
//    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
    
    
    
}

