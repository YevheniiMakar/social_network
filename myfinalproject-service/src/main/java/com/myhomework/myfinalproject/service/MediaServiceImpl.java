
package com.myhomework.myfinalproject.service;

import com.myhomework.myfinalproject.domain.BaseEntity.Medias;
import com.myhomework.myfinalproject.domain.DAO.MediasDao;
import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Component
public class MediaServiceImpl implements MediaService{
    @Autowired
    private MediasDao mediasDao;

    @Override
    public List<Medias> getMedias(int offset, int limit, String type) {
        return mediasDao.getMedias(offset, limit, type);
    }

    @Override
    public List<Medias> getMedias(int offset, int limit, String type, int idUsers) {
        return mediasDao.getMedias(offset, limit, type, idUsers);
    }

    @Override
    public Medias byId(Integer id) {
        
        Medias medias = mediasDao.byId(id);
        
        return medias;
    }

    @Override
    public Serializable save(Medias object) {
        return mediasDao.save(object);
    }

    @Override
    
    public void update(Medias object) {
       mediasDao.update(object);
    }

    @Override
    public void delete(Medias object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Medias> list() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    @Override
    public MediaObject byName(String name){
        return new MediaObject(mediasDao.byName(name));
    }
    @Override
    public int getMediasCount( int id, String type ){
        return mediasDao.getMediasCount(id, type);
    }
            
    
}
