/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myhomework.myfinalproject.service;

import com.myhomework.myfinalproject.domain.BaseEntity.Medias;
import java.util.List;

/**
 *
 * @author mag_o
 */
public interface MediaService extends BaseService<Medias, Integer>{
    public List<Medias> getMedias(int offset, int limit, String type);
    public List<Medias> getMedias(int offset, int limit, String type, int idUsers);
    public MediaObject byName(String name);
    public Medias byId(Integer id);
    public int getMediasCount( int id, String type );
}
