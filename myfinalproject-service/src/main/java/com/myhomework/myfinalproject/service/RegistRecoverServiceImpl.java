/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myhomework.myfinalproject.service;

import com.myhomework.myfinalproject.domain.BaseEntity.RegistRecover;
import com.myhomework.myfinalproject.domain.DAO.RegistRecoverDao;
import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Component
public class RegistRecoverServiceImpl implements RegistRecoverService{
    
    @Autowired
    private RegistRecoverDao registRecoverDao;

    @Override
    public RegistRecover byHashkey(String hashkey) {
        return registRecoverDao.byHashkey(hashkey);
        
    }
    @Override
    public RegistRecover byLogin(String login) {
        return registRecoverDao.byLogin(login);
        
    }
       

    @Override
    public RegistRecover byId(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Serializable save(RegistRecover object) {
        return registRecoverDao.save(object);
    }

    @Override
    public void update(RegistRecover object) {
        registRecoverDao.update(object);
    }

    @Override
    public void delete(RegistRecover object) {
        registRecoverDao.remove(object); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<RegistRecover> list() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
}
