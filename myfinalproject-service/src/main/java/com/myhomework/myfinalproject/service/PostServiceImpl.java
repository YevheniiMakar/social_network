
package com.myhomework.myfinalproject.service;

import com.myhomework.myfinalproject.domain.BaseEntity.Posts;
import com.myhomework.myfinalproject.domain.DAO.PostsDao;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


@Component
public class PostServiceImpl implements PostService{
    @Autowired
    private PostsDao postsDao;

    @Override
    public int getPostsCount() {
        return postsDao.getPostsCount();
    }
    @Override
    public int getPostsCountlist(int id) {
        return postsDao.getPostsCountlist(id);
    }

    @Override
    public int getPostsCountWall(int id) {
        return postsDao.getPostsCountWall(id);
    }

    @Override
    public List<PostObject> getPosts(int offset, int limit) {
        List<Posts> p = postsDao.getPosts(offset, limit);
        List <PostObject>postList = new ArrayList<>();
        for(int i=0; i<p.size(); i++ ){
            postList.add(new PostObject(p.get(i)));
        }
        return postList;
    }

    @Override
    public List<PostObject> getPosts(int offset, int limit, int id) {
        List<Posts> p =  postsDao.getPosts(offset, limit, id);
        List <PostObject>postList = new ArrayList<>();
        for(int i=0; i<p.size(); i++ ){
            postList.add(new PostObject(p.get(i)));
        }
        return postList;
    
    
    
    
    }

    @Override
    public List<PostObject> getPostInWall(int offset, int limit, int id) {
        
       List<Posts> p =  postsDao.getPostInWall(offset, limit, id);
        List <PostObject>postList = new ArrayList<>();
        for(int i=0; i<p.size(); i++ ){
            postList.add(new PostObject(p.get(i)));
        }
        
       return postList;
    }
    @Transactional
    @Override
    public List<Posts> getPostInWall( int id) {
        
       
        
       return postsDao.getPostInWall(id);
    }

    @Transactional
    @Override
    public Posts byId(Integer id) {
        return postsDao.byId(id);
    }

    @Override
    public Serializable save(Posts object) {
        return postsDao.save(object);
    }

    @Override
    @Transactional
    public void update(Posts object) {
       postsDao.update(object);
    }

    @Override
    public void delete(Posts object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Posts> list() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   
    
}
