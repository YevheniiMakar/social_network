
package com.myhomework.myfinalproject.service;

import com.myhomework.myfinalproject.domain.BaseEntity.Users;
import com.myhomework.myfinalproject.domain.DAO.UsersDao;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Component
public class UserServiceImpl implements UserService {

    @Autowired
    private UsersDao usersDao; 

    @Override
    @Transactional
    public Users byId(Integer id) {
        Users users = usersDao.byId(id);
        return users;
    }

    @Override
    public Serializable save(Users object) {
        return usersDao.save(object);
    }

    @Override
    public void update(Users object) {
        usersDao.update(object);
    }

    @Override
    public void delete(Users object) {
        usersDao.remove(object);
    }
    
    @Override
    public int getUsersCount(){
        return usersDao.getUsersCount();
    }

    @Override
    public List<Users> list() {
        return usersDao.list();
    }

    @Override
    public UserObject getUserById(Integer id) {
//        Users user = usersDao.byId(id);
        return new UserObject(usersDao.byId(id));
    }

//    @Override
//    public AdminUsersObject getFullUserById(Integer id) {
//        return new AdminUsersObject(usersDao.byId(id));
//    }
//
//    @Override
//    public AdminUsersListFrameObject getUsersFrame(int offset, int limit) {
//        int totalUsers = usersDao.getUsersCount();
//        List<Users> users = usersDao.getUsers(offset, limit);
//        return new AdminUsersListFrameObject(users, totalUsers);
//    }

    @Override
    @Transactional
    public Users byUserName(String username) {
        return usersDao.byUserName(username);
    }

    @Override
    public List<UserObject> getUsers(int offset, int limit) {
        List<Users> p =  usersDao.getUsers(offset, limit);
        List <UserObject>usersList = new ArrayList<>();
        for(int i=0; i<p.size(); i++ ){
            usersList.add(new UserObject(p.get(i)));
        }
        return usersList;
    }
/*}=
    @Override
    public List<Users> getFriend(int offset, int limit, int id) {
        return usersDao.getFriend(offset, limit, id);
    }

    @Override
    public List<Users> getSubscriber(int offset, int limit, int id) {
        return usersDao.getSubscriber(offset, limit, id);
    }

    @Override
    public List<Users> getSubscription(int offset, int limit, int id) {
        return usersDao.getSubscription(offset, limit, id);
    }*/

    @Override
    public List<UserObject> usersListWithoutFriends(int offset, int limit, int id) {
        List<Users> p = usersDao.usersListWithoutFriends(offset, limit, id);
        List <UserObject>usersList = new ArrayList<>();
        for(int i=0; i<p.size(); i++ ){
            usersList.add(new UserObject(p.get(i)));
        }
        return usersList;
    }
    

    @Override
    public List<UserObject> usersListFriends(int offset, int limit, int id) {
        List<Users> p = usersDao.usersListFriends(offset, limit, id);
        List <UserObject>usersList = new ArrayList<>();
        for(int i=0; i<p.size(); i++ ){
            usersList.add(new UserObject(p.get(i)));
        }
        return usersList;
    }
    @Override
    public int usersListWithoutFriendsCount(int id){
        return usersDao.usersListWithoutFriendsCount(id);
    }
    @Override
    public int usersListFriendsCount(int id){
        return usersDao.usersListFriendsCount(id);
    }
    
}

