/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myhomework.myfinalproject.service;

import com.myhomework.myfinalproject.domain.BaseEntity.Medias;
import java.util.Date;

/**
 *
 * @author mag_o
 */
public class MediaObject {
    private Integer id;
    private String name;
    
    private Date dataCreated;
    private String mediaType;
    private String status;
    private UserObject user;
    
    public MediaObject() {
        
    }

    public MediaObject(Medias medias) {
        this.id = medias.getId();
        this.name =medias.getName();
        
        this.dataCreated =medias.getDataCreated();
        this.mediaType = medias.getMediaType().toString();
        this.status = medias.getStatus().toString();
        this.user = new UserObject(medias.getUsers());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    

    public Date getDataCreated() {
        return dataCreated;
    }

    public void setDataCreated(Date dataCreated) {
        this.dataCreated = dataCreated;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public UserObject getUser() {
        return user;
    }

    public void setUser(UserObject user) {
        this.user = user;
    }
    

    
    
}
