/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myhomework.myfinalproject.service;

import com.myhomework.myfinalproject.domain.BaseEntity.Medias;
import com.myhomework.myfinalproject.domain.BaseEntity.Posts;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author mag_o
 */
public class PostObject {

    private Integer id;
    private String post;
    private Date dataCreated;
    private String status;
    private UserObject user;
    private String visibility;
    private List<MediaObject> mediasList;
    

    

    public PostObject() {
        
    }
    public PostObject(Posts posts) {
        this.id = posts.getId();
        this.post = posts.getPosts();
        this.dataCreated = posts.getDataCreated();
        this.status =posts.getStatus().toString();
        this.user = new UserObject(posts.getUsers());
        this.visibility = posts.getVisibility().toString();
        this.mediasList = new ArrayList<>();
        for(Medias p : posts.getMediasList()){
            this.mediasList.add(new MediaObject(p));
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public Date getDataCreated() {
        return dataCreated;
    }

    public void setDataCreated(Date dataCreated) {
        this.dataCreated = dataCreated;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public UserObject getUser() {
        return user;
    }

    public void setUser(UserObject user) {
        this.user = user;
    }
    public String getVisibility() {
        return visibility;
    }

    public void setVisibility(String visibility) {
        this.visibility= visibility;
    }
    public List<MediaObject> getMediasList() {
        return mediasList;
    }

    public void setMedia(List<MediaObject> mediasList) {
        this.mediasList = mediasList;
    }
    
  

}
