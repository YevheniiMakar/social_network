package com.myhomework.myfinalproject.web.controller;

import com.myhomework.myfinalproject.domain.BaseEntity.RegistRecover;
import com.myhomework.myfinalproject.domain.BaseEntity.Users;
import com.myhomework.myfinalproject.service.MediaService;
import com.myhomework.myfinalproject.service.RegistRecoverService;
import com.myhomework.myfinalproject.service.UserService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class Registration {

    @Autowired
    RegistRecoverService recoverService;
    @Autowired
    JavaMailSender mailSender;
    @Autowired
    UserService userService;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    MediaService mediasService;

    @RequestMapping(value = {"/register"}, method = RequestMethod.GET)
    public String register() {

        return "register";
    }

    @RequestMapping(value = {"/register"}, method = RequestMethod.POST)
    public String register(@RequestParam("firstname") String firstName, @RequestParam("lastname") String lastName,
            @RequestParam("password") String password, @RequestParam("email") String email,
            @RequestParam("passwordretype") String passwordretype, Model model) {
        List<String> errors = new ArrayList<>();

        if (isEmpty(firstName)) {
            errors.add("enter the Firstname");
        }
        if (isEmpty(lastName)) {
            errors.add("enter the Lastname");
        }
        if (isEmpty(email)) {
            errors.add("enter the Email");
        }
        if (isEmpty(password)) {
            errors.add("enter the Password");
        }
        if (!password.equals(passwordretype)) {
            errors.add("enter the Password");
        }
        if (!(email.contains("@"))) {
            errors.add("enter the valid Email");
        }
        if (password.length() < 6 || password.length() > 12) {
            errors.add("can not be less than 6 or more than 12 characters");
        }

        if (!errors.isEmpty()) {
            model.addAttribute("errors", errors);
            return "register";

        } else {
            Users u = userService.byUserName(email);
            RegistRecover r = recoverService.byLogin(email);
            if (u != null) {
                errors.add("This e-mail address has already been registered");

            }
            if (r != null) {

                errors.add("This e-mail address has already been registered. "
                        + "Check your email and follow the steps in the email");

            }

            if (errors.isEmpty()) {
                String pass = passwordEncoder.encode(password);
                System.out.println(pass);
                String hashkey = Long.toHexString(System.currentTimeMillis()) + Long.toHexString(System.currentTimeMillis()) + Long.toHexString(System.currentTimeMillis());

                RegistRecover rro = new RegistRecover(email, pass, firstName, lastName, hashkey);
                recoverService.save(rro);
                SimpleMailMessage message = new SimpleMailMessage();
                message.setFrom("maggogg88@gmail.com");
                message.setTo("maggogg88@gmail.com");
                //message.setTo(email);
                message.setSubject("registration");
                message.setText("to complete registration click here http://localhost:8084/myfinalproject-web/register/" + hashkey);
                mailSender.send(message);

                return "redirect:/complite";

            }
        }
        model.addAttribute("errors", errors);
        return "register";

    }

    @RequestMapping(value = {"/register/{hashkey}"}, method = RequestMethod.GET)
    public String registerand(@PathVariable(value = "hashkey") String hashkey) {
        RegistRecover rs = recoverService.byHashkey(hashkey);
        System.out.println(rs.getFirstname() + rs.getLastname());
        if (rs == null) {
            return "register";
        } else {

            Users us = new Users(rs.getLogin(), rs.getPassword(), rs.getFirstname(), rs.getLastname(), mediasService.byId(2));
            userService.save(us);
            recoverService.delete(rs);
        }
        return "redirect:/login";

    }


    private boolean isEmpty(String value) {
        if (value == null) {
            return true;
        }
        value = value.trim();
        return !(value.length() > 0);
    }

}
