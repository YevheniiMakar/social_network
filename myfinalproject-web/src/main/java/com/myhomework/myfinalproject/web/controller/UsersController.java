/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myhomework.myfinalproject.web.controller;

import com.myhomework.myfinalproject.domain.BaseEntity.Users;
import com.myhomework.myfinalproject.service.MediaService;
import com.myhomework.myfinalproject.service.PostObject;
import com.myhomework.myfinalproject.service.PostService;
import com.myhomework.myfinalproject.service.UserService;
import com.myhomework.myfinalproject.service.UserObject;

import com.myhomework.myfinalproject.web.counter.Counter;
import com.myhomework.myfinalproject.web.counter.CurrentUser;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UsersController {

    @Autowired
    UserService userService;
    @Autowired
    Counter counter;
    @Autowired
    PostService postService;
    @Autowired
    MediaService mediaService;
    @Autowired
    CurrentUser currentUser;

    @RequestMapping(value = {"/id{id}"}, method = RequestMethod.GET)
    public ModelAndView user(@PathVariable(value = "id") int id) {
        ModelAndView mv = new ModelAndView("user");
        UserObject userAuthObject = currentUser.userAuth();
        UserObject usersObject = userService.getUserById(id);

        int totalIt = postService.getPostsCountWall(id);

        int itemPage = 10;
        int nuPage = 1;
        int offst = counter.Offset(nuPage, itemPage);
        int pagecount = counter.PageCount(totalIt, itemPage);
        //int pagecount = 10;

        List<PostObject> postList = postService.getPostInWall(offst, itemPage, id);

        mv.addObject("user", usersObject);
        mv.addObject("currentuser", userAuthObject);
        mv.addObject("numePage", nuPage);
        mv.addObject("pagecount", pagecount);
        mv.addObject("postlist", postList);
        mv.addObject("videoview", mediaService.getMedias(0, 4, "VIDEO", id));
        mv.addObject("audioview", mediaService.getMedias(0, 4, "AUDIO", id));
        mv.addObject("imageview", mediaService.getMedias(0, 4, "PICTURE", id));
        
        String checkFriend="notfriend";
        for (int i=0; i<userAuthObject.getContactsList().size(); i++){
             if(userAuthObject.getContactsList().get(i)==id){
                 checkFriend = "friend";
             }
        }
        mv.addObject("checkfriend", checkFriend);
        

        return mv;
    }

    @RequestMapping(value = {"/id{id}/page{page}"}, method = RequestMethod.POST)
    public ModelAndView user(@PathVariable(value = "id") int id, @PathVariable(value = "page") int page) {
        ModelAndView mv = new ModelAndView("user");
        UserObject userAuthObject = currentUser.userAuth();
        UserObject usersObject = userService.getUserById(id);

        int totalIt = postService.getPostsCountWall(id);

        int itemPage = 10;
        int nuPage = page;
        int offst = counter.Offset(nuPage, itemPage);
        int pagecount = counter.PageCount(totalIt, itemPage);

        System.out.println(postService.getPostInWall(offst, itemPage, id));
        List<PostObject> postList = postService.getPostInWall(offst, itemPage, id);

        mv.addObject("user", usersObject);
        mv.addObject("currentuser", userAuthObject);
        mv.addObject("numePage", nuPage);
        mv.addObject("pagecount", pagecount);
        mv.addObject("postlist", postList);
        mv.addObject("videoview", mediaService.getMedias(1, 4, "VIDEO", id));
        mv.addObject("audioview", mediaService.getMedias(1, 4, "AUDIO", id));
        mv.addObject("imageview", mediaService.getMedias(1, 4, "PICTURE", id));

        return mv;
    }

    @RequestMapping(value = {"/id{id}/userslist_{list}"}, method = {RequestMethod.POST, RequestMethod.GET})
    public ModelAndView userList(@PathVariable(value = "id") int id, @PathVariable(value = "list") String list) {
        
        ModelAndView mv = new ModelAndView("userlist");
        UserObject userAuthObject = currentUser.userAuth();
        UserObject usersObject = userService.getUserById(id);
        int totalIt;
        int itemPage = 10;
        int nuPage = 1;
        int offset;
        int pageСount = 1;
        if (list.equals("users")) {
            totalIt = userService.usersListWithoutFriendsCount(id);
            offset = counter.Offset(nuPage, itemPage);
            pageСount = counter.PageCount(totalIt, itemPage);
            mv.addObject("userslist", userService.usersListWithoutFriends(offset, itemPage, id));
            
            System.out.println(" count" +pageСount +" offset" + offset);
            System.out.println("size"+userService.usersListWithoutFriends(offset, itemPage, id).size());

        } else if (list.equals("friends")) {
            totalIt = userService.usersListFriendsCount(id);
            offset = counter.Offset(nuPage, itemPage);
            pageСount = counter.PageCount(totalIt, itemPage);
            
            mv.addObject("userslist", userService.usersListFriends(offset, itemPage, id));
            System.out.println("22");
            System.out.println(" count" +pageСount +" offset" + offset);
            

        }

        mv.addObject("numePage", nuPage);
        mv.addObject("pagecount", pageСount);
        mv.addObject("user", usersObject);
        mv.addObject("currentuser", userAuthObject);
        mv.addObject("views", list);
        

        return mv;

    }

    @RequestMapping(value = {"/id{id}/userslist_{list}/page{page}"}, method = RequestMethod.POST)
    public ModelAndView userList(@PathVariable(value = "id") int id, @PathVariable(value = "page") int page, @PathVariable(value = "list") String list) {
        ModelAndView mv = new ModelAndView("userlist");
        UserObject userAuthObject = currentUser.userAuth();
        UserObject usersObject = userService.getUserById(id);
        int totalIt;
        int itemPage = 10;
        int nuPage = page;
        int offset;
        int pageСount = 1;
        if (list.equals("users")) {
            totalIt = userService.usersListWithoutFriendsCount(id);
            offset = counter.Offset(nuPage, itemPage);
            pageСount = counter.PageCount(totalIt, itemPage);
            mv.addObject("userslist", userService.usersListWithoutFriends(offset, itemPage, id));

        } else if (list.equals("friends")) {
            totalIt = userService.usersListWithoutFriendsCount(id);
            offset = counter.Offset(nuPage, itemPage);
            pageСount = counter.PageCount(totalIt, itemPage);
            mv.addObject("userslist", userService.usersListWithoutFriends(offset, itemPage, id));

        }

        mv.addObject("numePage", nuPage);
        mv.addObject("pagecount", pageСount);
        mv.addObject("user", usersObject);
        mv.addObject("currentuser", userAuthObject);
        mv.addObject("views", list);
        

        return mv;
    }
    @RequestMapping(value = "/id{id}/addfriends", method = RequestMethod.POST)
    @ResponseBody
    public String addFriend(@RequestParam("text") String idUser, @PathVariable(value = "id") int id){
        UserObject userAuthObject = currentUser.userAuth();
        UserObject addeUser = userService.getUserById(Integer.parseInt(idUser));
        Users user = userService.byId(userAuthObject.getId());
        
        user.getContactsList().add(userService.byId(Integer.parseInt(idUser)));
        userService.update(user);
        
        
        System.out.println("user id ="+idUser);
               

        
            

        return "complite";
    }

}
