/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myhomework.myfinalproject.web.emailcontroller;

import java.util.Properties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.transaction.annotation.EnableTransactionManagement;



@Configuration
@EnableTransactionManagement
//@ComponentScan("com.myhomework")
@PropertySource(name = "mail", value = "classpath:/mail.properties")
public class MailController {
    

    @Autowired
    private Environment environmentMail;

    @Bean
    public JavaMailSender getJavaMailSender() {
        JavaMailSender mailSender = new JavaMailSenderImpl();
        ((JavaMailSenderImpl)mailSender).setHost(environmentMail.getProperty("mail.host"));
        ((JavaMailSenderImpl)mailSender).setPort(new Integer(environmentMail.getProperty("mail.port")));

        ((JavaMailSenderImpl)mailSender).setUsername(environmentMail.getProperty("mail.username"));
        ((JavaMailSenderImpl)mailSender).setPassword(environmentMail.getProperty("mail.password"));

        Properties props = ((JavaMailSenderImpl)mailSender).getJavaMailProperties();
        props.put("mail.transport.protocol", environmentMail.getProperty("mail.transport.protocol"));
        props.put("mail.smtp.auth", environmentMail.getProperty("mail.smtp.auth"));
        props.put("mail.smtp.starttls.enable", environmentMail.getProperty("mail.smtp.starttls.enable"));
        props.put("mail.debug", "true");
        

        return mailSender;
    }

}
