
package com.myhomework.myfinalproject.web.config;

import com.myhomework.myfinalproject.service.UserService;
import com.myhomework.myfinalproject.domain.BaseEntity.Users;
import com.myhomework.myfinalproject.service.UserAuth;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;

@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter{
    @Autowired
    UserService userService;
    
    //public void init(WebSecurity web) {
        //web.ignoring().antMatchers("/");
        //web.ignoring().antMatchers("/login");
      //  web.ignoring().antMatchers("/admin");
    //    //web.ignoring().antMatchers("/registration");
  //  }
    
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/register").permitAll()
                .antMatchers("/register/**").permitAll()
                .antMatchers("/complite").permitAll()
                .antMatchers("/userslist").permitAll()
                .antMatchers("/userslist/**").permitAll()
                
                .antMatchers("/home/**")
                
                
                .access("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
                .antMatchers("/id{id}/**")
                
                .access("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
                
                //.access("hasRole('USER')")
                //.antMatchers("/admin/*")
                //.access("hasRole(ADMIN)")
                .and().formLogin().loginPage("/login")
//                .defaultSuccessUrl("/home")
                
                .and()
                .logout().logoutUrl("/logout");
       
}
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new PasswordEncoder() {
            private final ShaPasswordEncoder sha = new ShaPasswordEncoder();

            @Override
            public String encode(CharSequence cs) {
                return sha.encodePassword(cs.toString(), 1);
            }

            @Override
            public boolean matches(CharSequence cs, String string) {
              
//                System.out.println("pass cs == " + sha.encodePassword(cs.toString(), 1));
//                System.out.println("pass str == " + string);
               
                return (sha.encodePassword(cs.toString(), 1)).equals(string);
            }
        };
    }

    @Bean
    public DaoAuthenticationProvider daoAuthenticationProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setPasswordEncoder(passwordEncoder());
        authProvider.setUserDetailsService(userDetailsService());
        return authProvider;
    }

    @Override
    public UserDetailsService userDetailsService() {
        UserDetailsService detailsService = new UserDetailsService() {
            @Override
            public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
                Users user = null;
                try {
                    user = userService.byUserName(username);
                } catch (Exception ex) {
                    System.out.println("user not found or error");
                    ex.printStackTrace();
                    throw new UsernameNotFoundException(String.format("can't find user %s", username));
                }
                if (user == null) {
                    throw new UsernameNotFoundException(String.format("can't find user %s", username));
                }
                
                return (UserDetails) new UserAuth(user);
                
            }
        };
        
        
        return detailsService;
    }
    
    @Bean
    public AuthenticationDetailsSource authenticationDetailsSource (){
        return new WebAuthenticationDetailsSource(){
            //@Override
            public WebAuthenticationDetails builDetails (HttpServletRequest context){
                return new WebAuthenticationDetails (context);
            }
        };
    }
    @Autowired
    public void configureGlobal (AuthenticationManagerBuilder auth)throws Exception{
        auth.authenticationProvider(daoAuthenticationProvider());
    }

    
}
