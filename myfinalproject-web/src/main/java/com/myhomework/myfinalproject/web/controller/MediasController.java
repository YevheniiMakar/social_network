/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myhomework.myfinalproject.web.controller;

import com.myhomework.myfinalproject.service.MediaService;
import com.myhomework.myfinalproject.service.PostService;
import com.myhomework.myfinalproject.service.UserService;
import com.myhomework.myfinalproject.service.UserObject;
import com.myhomework.myfinalproject.web.counter.Counter;
import com.myhomework.myfinalproject.web.counter.CurrentUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MediasController {

    @Autowired
    UserService userService;
    @Autowired
    MediaService mediaService;
    @Autowired
    CurrentUser currentUser;
    @Autowired
    Counter counter;

    @RequestMapping(value = {"/id{id}/medias_{medias}"}, method = RequestMethod.GET)
    public ModelAndView medias(@PathVariable(value = "id") int id, @PathVariable(value = "medias") String medias) {
        ModelAndView mv =new ModelAndView("medias");
        String mediaType = null;
        UserObject userAuthObject = currentUser.userAuth();
        UserObject usersObject = userService.getUserById(id);
        
        if (medias.equalsIgnoreCase("images")) {
            mediaType = "PICTURE";
           
        } else if (medias.equalsIgnoreCase("audios")) {
            mediaType = "AUDIO";
            
        } else if (medias.equalsIgnoreCase("videos")) {
            mediaType = "VIDEO";
          
        }
        
        int totalIt = mediaService.getMediasCount(id, mediaType);
        
        int itemPage = 10;
        int nuPage = 1;
        int offst =counter.Offset(nuPage, itemPage);
        int pagecount = counter.PageCount(totalIt, itemPage);
        
        mv.addObject("medias", medias);
        mv.addObject("user", usersObject);
        mv.addObject("currentuser", userAuthObject);
        mv.addObject("numePage", nuPage);
        mv.addObject("pagecount", pagecount);
        mv.addObject("mediasview", mediaService.getMedias(offst, itemPage, mediaType, id));
        return mv;

    }
    @RequestMapping(value = {"/id{id}/medias_{medias}/page{page}"}, method = RequestMethod.GET)
    public ModelAndView medias(@PathVariable(value = "id") int id, @PathVariable(value = "medias") String medias, @PathVariable(value = "page") int page) {
        ModelAndView mv =new ModelAndView("medias");
        String mediaType = null;
        UserObject userAuthObject = currentUser.userAuth();
        UserObject usersObject = userService.getUserById(id);
        
        if (medias.equalsIgnoreCase("images")) {
            mediaType = "PICTURE";
          
            
            
        } else if (medias.equalsIgnoreCase("audio")) {
            mediaType = "AUDIO";
           
        } else if (medias.equalsIgnoreCase("video")) {
            mediaType = "VIDEO";
            
        }
        
        int totalIt = mediaService.getMediasCount(id, mediaType);
        
        int itemPage = 10;
        int nuPage = page;
        int offst =counter.Offset(nuPage, itemPage);
        int pagecount = counter.PageCount(totalIt, itemPage);
        
        mv.addObject("user", usersObject);
        mv.addObject("medias", medias);
        mv.addObject("currentuser", userAuthObject);
        mv.addObject("numePage", nuPage);
        mv.addObject("pagecount", pagecount);
        mv.addObject("mediasview", mediaService.getMedias(offst, itemPage, mediaType, id));
        return mv;

    }

}
