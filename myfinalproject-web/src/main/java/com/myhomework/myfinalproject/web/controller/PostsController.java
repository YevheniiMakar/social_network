/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myhomework.myfinalproject.web.controller;

import com.myhomework.myfinalproject.domain.BaseEntity.Medias;
import com.myhomework.myfinalproject.domain.BaseEntity.Posts;
import com.myhomework.myfinalproject.domain.BaseEntity.Users;
import com.myhomework.myfinalproject.service.MediaService;
import com.myhomework.myfinalproject.service.PostService;
import com.myhomework.myfinalproject.service.UserService;
import com.myhomework.myfinalproject.service.UserObject;
import com.myhomework.myfinalproject.web.counter.Counter;
import com.myhomework.myfinalproject.web.counter.CurrentUser;
import com.myhomework.myfinalproject.web.counter.PostCreater;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PostsController {

    @Autowired
    UserService userService;
    @Autowired
    PostService postService;
    @Autowired
    CurrentUser currentUser;
    @Autowired
    MediaService mediaService;
    @Autowired
    Counter counter;
    
    
    
   
    

    @RequestMapping(value = {"/id{id}/posts_{views}"}, method = RequestMethod.GET)
    public ModelAndView medias(@PathVariable(value = "id") int id, @PathVariable(value = "views") String views) {
        ModelAndView mv = new ModelAndView("posts");
        UserObject userAuthObject = currentUser.userAuth();
        UserObject usersObject = userService.getUserById(id);
        int totalIt;
        int itemPage = 10;
        int nuPage = 1;
        int offset;
        int pageСount = 1;

        if (views.equals("post")) {
            totalIt = postService.getPostsCountlist(id);
            offset = counter.Offset(nuPage, itemPage);
            pageСount = counter.PageCount(totalIt, itemPage);
            mv.addObject("postlist", postService.getPosts(offset, itemPage, id));
        } else if (views.equals("allnews")) {
            totalIt = postService.getPostsCount();
            offset = counter.Offset(nuPage, itemPage);
            pageСount = counter.PageCount(totalIt, itemPage);
            mv.addObject("postlist", postService.getPosts(offset, itemPage));
        }

        mv.addObject("numePage", nuPage);
        mv.addObject("pagecount", pageСount);
        mv.addObject("user", usersObject);
        mv.addObject("currentuser", userAuthObject);
        mv.addObject("views", views);
        return mv;

    }

    @RequestMapping(value = {"/id{id}/posts_{views}/page{page}"}, method = RequestMethod.GET)
    public ModelAndView medias(@PathVariable(value = "id") int id, @PathVariable(value = "views") String views, @PathVariable(value = "page") int nuPage) {
        ModelAndView mv = new ModelAndView("posts");
        UserObject userAuthObject = currentUser.userAuth();
        UserObject usersObject = userService.getUserById(id);
        int totalIt;
        int itemPage = 10;
        //int nuPage = 1;
        int offset;
        int pageСount = 1;

        if (views.equals("post")) {
            totalIt = postService.getPostsCountlist(id);
            offset = counter.Offset(nuPage, itemPage);
            pageСount = counter.PageCount(totalIt, itemPage);
            mv.addObject("postlist", postService.getPosts(offset, itemPage, id));
        } else if (views.equals("allnews")) {
            totalIt = postService.getPostsCount();
            offset = counter.Offset(nuPage, itemPage);
            pageСount = counter.PageCount(totalIt, itemPage);
            mv.addObject("postlist", postService.getPosts(offset, itemPage));
        }

        mv.addObject("numePage", nuPage);
        mv.addObject("pagecount", pageСount);
        mv.addObject("user", usersObject);
        mv.addObject("currentuser", userAuthObject);
        mv.addObject("views", views);
        return mv;

    }
    
    @RequestMapping(value = "/id{id}/addposttowall", method = RequestMethod.POST)
    @ResponseBody
    public String addPostToWall(@RequestParam("text") String idPost, @PathVariable(value = "id") int id) {
        UserObject userAuthObject = currentUser.userAuth();
        Posts post = postService.byId(Integer.parseInt(idPost));
        Users user = userService.byId(userAuthObject.getId());
        user.getWallList().add(post);
        userService.update(user);
        
          


        

        System.out.println("user id =" + idPost);

        return "complite";
    }

    @RequestMapping(value = "/id{id}/addpost", method = RequestMethod.POST, headers = {"Content-type=application/json"})
    @ResponseBody
    public String addPost(@RequestBody PostCreater cr, @PathVariable(value = "id") int id) {

        Users u = userService.byId(currentUser.userAuth().getId());

        Posts posts = new Posts(cr.getText(), u);
        List<Medias> lm = new ArrayList<>();
        for (int i = 0; i < cr.getMediasObjects().length; i++) {
            lm.add(mediaService.byId(cr.getMediasObjects()[i].getId()));
            System.out.println(cr.getMediasObjects().length + "       " + i);
        }

        posts.setMediasList(lm);

        posts.getWallList().add(u);
        postService.save(posts);

        return "complite";
    }
}
