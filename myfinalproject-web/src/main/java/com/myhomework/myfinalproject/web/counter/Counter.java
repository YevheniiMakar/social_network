
package com.myhomework.myfinalproject.web.counter;


import org.springframework.stereotype.Component;

@Component
public class Counter {
   
    public int PageCount(int totalItem, int itemInPage){
        int pc = (int)(Math.ceil((double)totalItem/(double)itemInPage));
        if(pc == 0){
            return 1;
        }
        return pc;
    }
    
    public int Offset(int numPage, int itemInPage){
        
        return ((numPage - 1) * itemInPage);
    }
    
}
