package com.myhomework.myfinalproject.web.controller;

import com.myhomework.myfinalproject.domain.BaseEntity.Medias;
import com.myhomework.myfinalproject.domain.BaseEntity.Posts;
import com.myhomework.myfinalproject.domain.BaseEntity.RegistRecover;
import com.myhomework.myfinalproject.domain.BaseEntity.Users;
import com.myhomework.myfinalproject.service.MediaService;
import com.myhomework.myfinalproject.service.MediaObject;
import com.myhomework.myfinalproject.service.PostService;
import com.myhomework.myfinalproject.service.RegistRecoverObject;
import com.myhomework.myfinalproject.service.RegistRecoverService;
import com.myhomework.myfinalproject.service.UserAuth;
import com.myhomework.myfinalproject.service.UserService;
import com.myhomework.myfinalproject.service.UserObject;
import com.myhomework.myfinalproject.web.counter.CurrentUser;
import com.myhomework.myfinalproject.web.counter.PostCreater;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class HomeController {
    @Autowired
    RegistRecoverService recoverService;
    
    @Autowired
    UserService usersService;
    
    @Autowired
    MediaService mediaService;
    
    @Autowired
    PostService postService;
    @Autowired
    CurrentUser currentUser;

    @RequestMapping(value = {"/"},
            method = RequestMethod.GET
    )
    public ModelAndView home() {

        ModelAndView mv = new ModelAndView("index");
        return mv;

    }

    @RequestMapping(value = {"/home"}, method = RequestMethod.GET)
    public String home2() {
        
        UserObject user = currentUser.userAuth();

        //String s = null;
        if (user != null) {

            if (user.getRole().equals("ROLE_USER")) {
                int id = user.getId();
                return "redirect:/id" + id;

            }
            if (user.getRole().equals("ROLE_ADMIN")) {
                return "redirect:welcom";
            }
        } 
        return "index";
        
    }

    @RequestMapping(value = {"/login"}, method = RequestMethod.GET)
    public ModelAndView LoginPage() {
        ModelAndView mv = new ModelAndView("login");
        return mv;

    }

    /**
     *
     * @param cr
     * @param id
     * @return
     */
    

    
       
}
