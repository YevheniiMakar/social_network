/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myhomework.myfinalproject.web.controller;

import com.myhomework.myfinalproject.domain.BaseEntity.Medias;
import com.myhomework.myfinalproject.domain.BaseEntity.Users;
import com.myhomework.myfinalproject.service.MediaService;
import com.myhomework.myfinalproject.service.UserService;
import com.myhomework.myfinalproject.service.MediaObject;
import com.myhomework.myfinalproject.web.counter.CurrentUser;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class FileUploadController {

    @Autowired
    MediaService mediaService;
    @Autowired
    UserService userService;
    @Autowired
    CurrentUser currentUser;

    private static final Logger logger = LoggerFactory
            .getLogger(FileUploadController.class);

//    @RequestMapping(value="/upload", method=RequestMethod.GET)
//    public @ResponseBody String provideUploadInfo() {
//        return "Вы можете загружать файл с использованием того же URL.";
    @ResponseBody
    @Transactional
    @RequestMapping(value = "/id{id}/addmedias", method = RequestMethod.POST)
    public AjaxResponceBody handleFileUpload(@RequestParam("uploadfile") MultipartFile file, Model model, @PathVariable(value = "id") int id) {

        Users u = userService.byId(currentUser.userAuth().getId());

        AjaxResponceBody result;

        if (!file.isEmpty()) {

            Medias m = mediaNameType(file.getOriginalFilename());
            System.out.println(m.getName());
            m.setUsers(u);

            try {

                byte[] bytes = file.getBytes();

                // Creating the directory to store file
                //String rootPath = System.getProperty("catalina.home");
                File dir = new File("/home/yevhenii/temppro/");

                if (!dir.exists()) {
                    dir.mkdirs();
                }

                // Create the file on server
                File serverFile = new File(dir.getAbsolutePath()
                        + File.separator + m.getName());
                BufferedOutputStream stream = new BufferedOutputStream(
                        new FileOutputStream(serverFile));
                stream.write(bytes);
                stream.close();

                logger.info("Server File Location="
                        + serverFile.getAbsolutePath());
                ;

                mediaService.save(m);
                MediaObject mo = mediaService.byName(m.getName());
                result = new AjaxResponceBody("upload complite", mo);

                return result;
            } catch (Exception e) {
                result = new AjaxResponceBody("You failed to upload " + file.getOriginalFilename() + " => " + e.getMessage(), null);
                return result;
            }
        } else {
            result = new AjaxResponceBody("You failed to upload " + file.getOriginalFilename()
                    + " because the file was empty.", null);
        }
        return result;
    }

    public Medias mediaNameType(String fileName) {

        Medias medias;
        fileName = fileName.toLowerCase();

        if (fileName.endsWith(".jpg")) {

            medias = new Medias(named() + ".jpg", Medias.MediaType.PICTURE);
        } else if (fileName.endsWith(".gif")) {
            medias = new Medias(named() + ".gif", Medias.MediaType.PICTURE);
        } else if (fileName.endsWith(".png")) {
            medias = new Medias(named() + ".png", Medias.MediaType.PICTURE);
        } else if (fileName.endsWith(".avi")) {
            medias = new Medias(named() + ".avi", Medias.MediaType.VIDEO);
        } else if (fileName.endsWith(".mp4")) {
            medias = new Medias(named() + ".mp4", Medias.MediaType.VIDEO);
        } else if (fileName.endsWith(".mp3")) {
            medias = new Medias(named() + ".mp3", Medias.MediaType.AUDIO);
        } else {

            medias = null;

        }
        return medias;

    }

    public String named() {

        return (Long.toHexString(System.currentTimeMillis())
                + Long.toHexString(System.currentTimeMillis())
                + Long.toHexString(System.currentTimeMillis()));

    }

    private static class AjaxResponceBody {

        private String message;
        private MediaObject media;

        public AjaxResponceBody() {

        }

        public AjaxResponceBody(String message, MediaObject media) {
            this.message = message;
            this.media = media;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public MediaObject getMedia() {
            return media;
        }

        public void setMedia(MediaObject media) {
            this.media = media;
        }

    }

}
