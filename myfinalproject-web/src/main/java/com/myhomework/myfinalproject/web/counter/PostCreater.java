/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myhomework.myfinalproject.web.counter;

import com.myhomework.myfinalproject.service.MediaObject;

/**
 *
 * @author yevhenii
 */
public class PostCreater {
        private String text;
        private MediaObject[] mediasObjects;
        
        

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public MediaObject[] getMediasObjects() {
            return mediasObjects;
        }

        public void setMediasObjects(MediaObject[] mediasObjects) {
            this.mediasObjects = mediasObjects;
        }
    
}
