/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myhomework.myfinalproject.web.counter;

import com.myhomework.myfinalproject.service.UserAuth;
import com.myhomework.myfinalproject.service.UserService;
import com.myhomework.myfinalproject.service.UserObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class CurrentUser {
    @Autowired
    UserService userService;
    
    public UserObject userAuth() {
        UserAuth userAuth = null;

        if (!(SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString().equals("anonymousUser"))) {
            userAuth = (UserAuth) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        }
        
        return userService.getUserById(userAuth.getId());
    }
    
}
