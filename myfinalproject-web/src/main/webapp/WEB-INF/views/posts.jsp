<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>AdminLTE 2 | user images</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="/myfinalproject-web/static/bower_components/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="/myfinalproject-web/static/bower_components/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="/myfinalproject-web/static/bower_components/Ionicons/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="/myfinalproject-web/static/dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="/myfinalproject-web/static/dist/css/skins/_all-skins.min.css">
        <script src="/myfinalproject-web/static/plugins/jquery/jquery-3.2.1.min.js"></script>
        <script src="/myfinalproject-web/static/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="/myfinalproject-web/static/plugins/bootpag/jquery.bootpag.min.js"></script>
        <!--<script src="http://botmonster.com/jquery-bootpag/jquery.bootpag.js"></script>-->


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- add new post -->

        <style>

            .img-container-add-outer {
                position: relative;
                float: left;
                width: 150px;
                height: 150px;
                margin: 10px;
                border: 1px solid #CCC;
            }

            .img-container-add-inner {
                position: absolute;
                top: 5%;
                left: 5%;
                width: 130px;
                height: 130px;

                overflow: hidden;
            }
            .img-container-add-span {
                display: block;
                position: absolute;
                left: 50%;
                width: 500em;
                height: 100%;
                margin-left: -250em;
                text-align: center;
            }
            .img-content-add-img {
                height: 100%;
                vertical-align: bottom;
            }

        </style>


        <!-- add new post -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <!--<script src="https://code.jquery.com/jquery-3.2.1.min.js" type="text/javascript"></script>-->
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">

            <header class="main-header">
                <!-- Logo -->
                <a href="/home" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>A</b>LT</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>Admin</b>LTE</span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="/myfinalproject-web/file/${currentuser.photo}" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">

                            <a href="/myfinalproject-web/id${currentuser.id}"><p>${currentuser.firstname} ${currentuser.lastname}</p> </a>
                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu" data-widget="tree">
                        <li class="header">MAIN NAVIGATION</li>
                        <li>
                            <a href="/myfinalproject-web/id${currentuser.id}">
                                <!--<i class="fa fa-files-o"></i>-->
                                <span>My page</span>
                            </a>
                        </li>

                        <li>
                            <a href="/myfinalproject-web/id${currentuser.id}/friends">
                                <i class="fa fa-files-o"></i>
                                <span>Friend</span>
                                <span class="pull-right-container">
                                    <span class="label label-primary pull-right">1112</span>
                                </span>
                            </a>
                        </li>
                        <!--                        <li class="treeview">
                                                    <a href="#">
                                                        <i class="fa fa-files-o"></i>
                                                        <span>Massage</span>
                                                        <span class="pull-right-container">
                                                            <span class="label label-primary pull-right">1122</span>
                                                        </span>
                                                    </a>
                                                </li>-->
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-dashboard"></i> <span>Posts</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="/myfinalproject-web/id${currentuser.id}/posts_post"><i class="fa fa-circle-o"></i> My posts</a></li>
                                <li><a href="/myfinalproject-web/id${currentuser.id}/posts_allnews"><i class="fa fa-circle-o"></i> News</a></li>

                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-dashboard"></i> <span>Users</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="/myfinalproject-web/id${currentuser.id}/userslist_friends"><i class="fa fa-circle-o"></i> Friends</a></li>
                                <li><a href="/myfinalproject-web/id${currentuser.id}/userslist_users"><i class="fa fa-circle-o"></i> Users</a></li>

                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-dashboard"></i> <span>Media</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="/myfinalproject-web/id${currentuser.id}/medias_images"><i class="fa fa-circle-o"></i> Images</a></li>
                                <li><a href="/myfinalproject-web/id${currentuser.id}/medias_audios"><i class="fa fa-circle-o"></i> Audio</a></li>
                                <li><a href="/myfinalproject-web/id${currentuser.id}/medias_videos"><i class="fa fa-circle-o"></i> Video</a></li>
                            </ul>
                        </li>
                        <li class=>
                            <a href="/myfinalproject-web/id${currentuser.id}/profile">
                                <i class="fa fa-files-o"></i>
                                <span>Profile</span>
                            </a>
                        </li>

                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <section class="content">

                    <div class="row">

                        <!-- /.col -->
                        <div class="col-md-9 box box-primary" style="border-radius: 15px 15px 15px 15px">


                            <div class="box-body">
                                <div class="box-body margin-center">
                                    <c:forEach var="postl" items="${postlist}" >
                                        <div class="post">
                                            <div class="user-block">
                                                <img class="img-circle img-bordered-sm" src="/myfinalproject-web/file/${postl.user.photo}" alt="user image">
                                                <span class="username">
                                                    <a href="#">${postl.user.firstname} ${postl.user.lastname}</a>
                                                    <a href="#" class="pull-right btn-box-tool"><i class="fa fa-times"></i></a>
                                                </span>
                                                <!--<span class="description">Shared publicly - 7:30 PM today</span>-->
                                            </div>
                                            <!-- /.user-block -->
                                            <p>${postl.post}</p>
                                            
                                            <div class="container-fluid">
                                             <c:forEach var="postmp" items="${postl.mediasList}" >

                                                <c:if test="${postmp.mediaType eq 'PICTURE'}">
                                                    <div class="img-container-add-outer">
                                                        <div class="img-container-add-inner">
                                                            <div class="img-container-add-span">
                                                                <img class="img-content-add-img" src="/myfinalproject-web/file/${postmp.name}" alt="альтернативный текст">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </c:if>
                                            </c:forEach>
                                            </div>
                                        

                                            <table>
                                                <c:forEach var="postma" items="${postl.mediasList}" >

                                                    <c:if test="${postma.mediaType eq 'AUDIO'}">

                                                        <tr>
                                                            <td>${postm.name}</td>
                                                            <td>
                                                                <audio controls>
                                                                    <source src="/myfinalproject-web/file/${postma.name}" type="audio/mpeg">
                                                                    Тег audio не поддерживается вашим браузером. 
                                                                    <a href="/myfinalproject-web/file/${postma.name}">Скачайте музыку</a>.
                                                                </audio>
                                                            </td>

                                                        </tr>
                                                    </c:if>
                                                </c:forEach>
                                            </table>
                                            <ul class="list-inline">
                                                <li><a href="#" class="link-black text-sm" onclick="addPostToWall(${postl.id})"><i class="fa fa-share margin-r-5"></i> Share</a></li>
                                            </ul>
                                            <p>${postl.id}</p>

                                           

                                            


                                        </div>
                                    </c:forEach>

                                </div>
                            </div>


                            <div class="box-body">

                                <div id="page-selection"></div>

                            </div>
                        </div>
                        <!-- /.row -->

                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 2.4.0
                </div>
                <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
                reserved.
            </footer>


            <!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>
        </div>
        <!-- ./wrapper -->

        <!-- jQuery 3 -->
        <!--загружаем файлы на сервер-->
        <script>
            // init bootpag
            $('#page-selection').bootpag({

                total: ${pagecount},
                page:${numePage},
                href: "/myfinalproject-web/id${user.id}/posts_${views}/page{{number}}"
            }).on("page", function (event, /* page number here */ num) {
                $("#content").html("Insert content"); // some ajax content loading...
            });
        </script>
        <script>
            function addPostToWall(obj) {
                alert("раз")
                var id = obj;
                alert(id)
                $.ajax({
                    url: '/myfinalproject-web/id${currentuser.id}/addposttowall',
                    type: 'POST',
                    data: ({
                        text: id
                    }),
                    success: function (data) {

                        alert(data);

                    }
                });
            }
        </script>

        <script>
            var mediasAudio = new Array();
            var mediasVideo = new Array();
            var mediasImages = new Array();
            var mediasAll = new Array();


            $(document).ready(function () {
                $("#upload-file-input").on("change", uploadFile);

            });
            function uploadFile() { // функция на загрузку файлов на серер
                var length = mediasAll.length;
                if (length <= 10) {

                    /* сам запрос на загрузку данных на сервер*/
                    $.ajax({
                        url: '/myfinalproject-web/id${currentuser.id}/addmedias',
                        type: 'POST',
                        data: new FormData($("#upload-file-form")[0]),
                        enctype: 'multipart/form-data',
                        processData: false,
                        contentType: false,
                        cache: false,
                        dataType: 'json',
                        success: function (data) {
                            alert(JSON.stringify(data.media));
                            sortMedia(data.media);

                            viewAudio();
                            viewVideo();
                            viewImages();

                            $("#upload-file-message").text("File succesfully uploaded");
                            $("#upload-file-message").text();
                        },
                        error: function () {
                            // Handle upload error
                            $("#upload-file-message").text(
                                    "File not uploaded (perhaps it's too much big)");
                        }
                    });
                }
                ;
            }
            ;
            function sortMedia(medias) {
                if (medias.mediaType === 'PICTURE') {
                    mediasImages.push(medias);
                    mediasAll.push(medias);

                }
                if (medias.mediaType === 'VIDEO') {
                    mediasVideo.push(medias);
                    mediasAll.push(medias);
                }
                if (medias.mediaType === 'AUDIO') {
                    mediasAudio.push(medias);
                    mediasAll.push(medias);

                }
            }
            ;
            function viewAudio() {

                var res = '';
                for (var i = 0; i < mediasAudio.length; i++) {
                    var str = '<tr><td>' + mediasAudio[i].name + '</td><td><audio controls><source src="/myfinalproject-web/file/' + mediasAudio[i].name + '" type="audio/mpeg"></audio></td></tr>';
                    res = res + str;
                    //<img src="images/redcat.jpg" alt="Задана ширина" width="200">
                }
                $("#view-medias-file-audio").html(res);
            }
            ;
            function viewVideo() {

                var res = '';

                for (var i = 0; i < mediasVideo.length; i++) {

                    var str = '<div class="img-container-add-outer"><div class="img-container-add-inner"><span class="img-container-add-span"><a href="/myfinalproject-web/file' + video.jpeg + '"><img class="img-content-add-img" src="/myfinalproject-web/file/' + mediasImages[i].name + '" alt="альтернативный текст"></span></div></div>';

                    res = res + str;
                }
                $("#view-medias-file").html(res);
            }
            ;
            function viewImages() {


                var res = '';

                for (var i = 0; i < mediasImages.length; i++) {


                    var str = '<div class="img-container-add-outer"><div class="img-container-add-inner"><span class="img-container-add-span"><img class="img-content-add-img" src="/myfinalproject-web/file/' + mediasImages[i].name + '" alt="альтернативный текст"></span></div></div>';

                    res = res + str;
                    //"<img src= \"/myfinalproject-web/file/\""+mediasAudio[i].name+"\" width=\"200\">";

                    //<!--<img src="images/redcat.jpg" alt="Задана ширина" width="200">--> 
                }
                //$("#view-medias-file-video").html("<img src= \"/myfinalproject-web/file/"+mediasImages[0].name+"\" width=\"200\">");
                $("#view-medias-file-images").html(res);
            }
            ;



            function doAddPost() { // функция предачи текстового содержимого поста + информации я о файлах прикрепленных к посту

                var text = $("#addPost").val();
                var creatPost = {"text": text, "mediasObjects": mediasAll};
                //mediasAll = Json.stri
                /*функция передачи текста и файлов*/
                $.ajax({
                    url: '/myfinalproject-web/id${currentuser.id}/addpost',
                    type: 'POST',
                    //dataType: 'json',
                    contentType: 'application/json',
                    data: JSON.stringify(creatPost),
                    success: function (data) {

                        var result = data;
                        $("#result_text").text(result);//не уверен что правильно передаю JSON на сервер
                    }
                });
            }
            ;
        </script>
        <!--пагинация-->


        <script src="/myfinalproject-web/static/bower_components/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="/myfinalproject-web/static/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="/myfinalproject-web/static/bower_components/fastclick/lib/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="/myfinalproject-web/static/dist/js/adminlte.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="/myfinalproject-web/static/dist/js/demo.js"></script>
        <script src="/myfinalproject-web/static/bower_components/ckeditor/ckeditor.js"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src="/myfinalproject-web/static/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>

    </body>
</html>
