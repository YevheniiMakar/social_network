<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>AdminLTE 2 | user images</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="/myfinalproject-web/static/bower_components/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="/myfinalproject-web/static/bower_components/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="/myfinalproject-web/static/bower_components/Ionicons/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="/myfinalproject-web/static/dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="/myfinalproject-web/static/dist/css/skins/_all-skins.min.css">
        <script src="/myfinalproject-web/static/plugins/jquery/jquery-3.2.1.min.js"></script>
        <script src="/myfinalproject-web/static/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="/myfinalproject-web/static/plugins/bootpag/jquery.bootpag.min.js"></script>

        <!--<script src="http://botmonster.com/jquery-bootpag/jquery.bootpag.js"></script>-->


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- add new post -->

        <style>

            .img-container-add-outer {
                position: relative;
                float: left;
                width: 150px;
                height: 150px;
                margin: 10px;
                border: 1px solid #CCC;
            }

            .img-container-add-inner {
                position: absolute;
                top: 5%;
                left: 5%;
                width: 130px;
                height: 130px;

                overflow: hidden;
            }
            .img-container-add-span {
                display: block;
                position: absolute;
                left: 50%;
                width: 500em;
                height: 100%;
                margin-left: -250em;
                text-align: center;
            }
            .img-content-add-img {
                height: 100%;
                vertical-align: bottom;
            }
            .main-header{
                background-color: blue;
                margin-bottom: 15px;
            }
            .user_info{
                height: 225px;
            }
            .flat_button {
                padding: 7px 16px 8px;
                margin-top: 85px;
                margin-left: 10px;
                font-size: 12.5px;
                display: inline-block;
                zoom: 1;
                cursor: pointer;
                white-space: nowrap;
                outline: none;
                vertical-align: top;
                line-height: 15px;
                text-align: center;
                text-decoration: none;
                background: none;
                background-color: #5181b8;
                color: #fff;
                border: 0;
                border-radius: 4px;
                box-sizing: border-box;
                font-family: "Open Sans";
            }
            .button_wide button, .flat_button.button_wide {
                min-width: 165px;
                padding-left: 3px;
                padding-right: 3px;
            }
            .flat_button:hover {
                background-color: #07356a;
                text-decoration: none;
                color: #b8babc;
            }
            .block_ava:hover {
                -webkit-transform: scale(1.1);
                -moz-transform: scale(1.1);
                -o-transform: scale(1.1);
            }
            .block_ava{
                -moz-transition: all 1s ease-out;
                -o-transition: all 1s ease-out;
                -webkit-transition: all 1s ease-out;
                cursor: pointer;
            }
            .img-ava{
                margin-top: 30px;
                width: 170px;
                border-radius: 100%;
            }
            .name_user a{
                color: #2a5885;
                text-decoration: none;
                cursor: pointer;
                font-size: 1.5em;
                font-weight: bold;
                font-family: "Open Sans";
            }
            .name_user{
                margin-top: 80px;
                margin-left: 30px;
            }

            @media screen and (max-width: 575px) {
                .block_info{
                    text-align: center;
                }
                .name_user{
                    margin-top: 20px;
                    margin-left: 0;
                }
                .flat_button{
                    margin-top: 20px;
                    margin-left: 0;
                }
                .img-ava{
                    margin-top: 30px;
                    margin-left: 0;
                }
            }

        </style>


        <!-- add new post -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <!--<script src="https://code.jquery.com/jquery-3.2.1.min.js" type="text/javascript"></script>-->
        <script src="/myfinalproject-web/static/style.css"></script>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">

            <header class="main-header">
                <!-- Logo -->
                <a href="/home" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>A</b>LT</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>Admin</b>LTE</span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="/myfinalproject-web/file/${currentuser.photo}" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">

                            <a href="/myfinalproject-web/id${currentuser.id}"><p>${currentuser.firstname} ${currentuser.lastname}</p> </a>
                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu" data-widget="tree">
                        <li class="header">MAIN NAVIGATION</li>
                        <li>
                            <a href="/myfinalproject-web/id${currentuser.id}">
                                <!--<i class="fa fa-files-o"></i>-->
                                <span>My page</span>
                            </a>
                        </li>

                        <li>
                            <a href="/myfinalproject-web/id${currentuser.id}/friends">
                                <i class="fa fa-files-o"></i>
                                <span>Friend</span>
                                <span class="pull-right-container">
                                    <span class="label label-primary pull-right">1112</span>
                                </span>
                            </a>
                        </li>
                        <!--                        <li class="treeview">
                                                    <a href="#">
                                                        <i class="fa fa-files-o"></i>
                                                        <span>Massage</span>
                                                        <span class="pull-right-container">
                                                            <span class="label label-primary pull-right">1122</span>
                                                        </span>
                                                    </a>
                                                </li>-->
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-dashboard"></i> <span>Users</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="/myfinalproject-web/id${currentuser.id}/userslist_friends"><i class="fa fa-circle-o"></i> Friends</a></li>
                                <li><a href="/myfinalproject-web/id${currentuser.id}/userslist_users"><i class="fa fa-circle-o"></i> Users</a></li>

                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-dashboard"></i> <span>Posts</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="/myfinalproject-web/id${currentuser.id}/posts_post"><i class="fa fa-circle-o"></i> My posts</a></li>
                                <li><a href="/myfinalproject-web/id${currentuser.id}/posts_allnews"><i class="fa fa-circle-o"></i> News</a></li>

                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-dashboard"></i> <span>Media</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="/myfinalproject-web/id${currentuser.id}/medias_images"><i class="fa fa-circle-o"></i> Images</a></li>
                                <li><a href="/myfinalproject-web/id${currentuser.id}/medias_audios"><i class="fa fa-circle-o"></i> Audio</a></li>
                                <li><a href="/myfinalproject-web/id${currentuser.id}/medias_videos"><i class="fa fa-circle-o"></i> Video</a></li>
                            </ul>
                        </li>
                        <li class=>
                            <a href="/myfinalproject-web/id${currentuser.id}/profile">
                                <i class="fa fa-files-o"></i>
                                <span>Profile</span>
                            </a>
                        </li>

                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <section class="content">

                    <div class="row">

                        <!-- /.col -->
                        <div class="col-md-9 box box-primary" style="border-radius: 15px 15px 15px 15px">


                            <div class="box-body">
                                <div class="box-body margin-center">
                                    <c:forEach var="usersl" items="${userslist}" >
                                        <div class="container-fluid">
                                            <div class="row user_info">
                                                <div class="col-xs-12 col-sm-4 col-md-2 block_ava block_info">
                                                    <img src="/myfinalproject-web/file/${usersl.photo}" class="img-ava" alt="edvard">
                                                </div>
                                                <div class="col-xs-12 col-sm-4 col-md-3 block_info">
                                                    <div class="name_user"><a href="/myfinalproject-web/id${usersl.id}">${usersl.firstname} ${usersl.lastname}</a></div>
                                                </div>
                                                <c:if test="${views eq 'users'}">
                                                    <div class="col-xs-12 col-sm-3 col-md-2 block_info">
                                                        <button class="flat_button button_wide" onclick="addFriend(${usersl.id})">Add to friends</button>
                                                    </div>
                                                </c:if>

                                            </div>
                                        </div>



                                    </c:forEach>

                                </div>
                            </div>


                            <div class="box-body">

                                <div id="page-selection"></div>

                            </div>
                        </div>
                        <!-- /.row -->

                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 2.4.0
                </div>
                <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
                reserved.
            </footer>


            <!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>
        </div>
        <!-- ./wrapper -->

        <!-- jQuery 3 -->
        <!--загружаем файлы на сервер-->
        <script>
            // init bootpag
            $('#page-selection').bootpag({

                total: ${pagecount},
                page:${numePage},
                href: "/myfinalproject-web/id${user.id}/posts_${views}/page{{number}}"
            }).on("page", function (event, /* page number here */ num) {
                $("#content").html("Insert content"); // some ajax content loading...
            });
        </script>
        <script>
            function addFriend(obj) {
                alert("раз")
                var id = obj;
                alert(id)
                $.ajax({
                    url: '/myfinalproject-web/id${currentuser.id}/addfriends',
                    type: 'POST',
                    data: ({
                        text: id
                    }),
                    success: function (data) {

                       alert(data);
                        
                    }
                });
            }
        </script>






        <script src="/myfinalproject-web/static/bower_components/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="/myfinalproject-web/static/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="/myfinalproject-web/static/bower_components/fastclick/lib/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="/myfinalproject-web/static/dist/js/adminlte.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="/myfinalproject-web/static/dist/js/demo.js"></script>
        <script src="/myfinalproject-web/static/bower_components/ckeditor/ckeditor.js"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src="/myfinalproject-web/static/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>

    </body>
</html>

