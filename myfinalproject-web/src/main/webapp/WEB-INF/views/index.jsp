<%-- 
    Document   : index
    Created on : 02.10.2017, 1:23:55
    Author     : mag_o
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="static/bower_components/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="static/bower_components/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="static/bower_components/Ionicons/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="static/dist/css/AdminLTE.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="static/plugins/iCheck/square/blue.css">
    </head>
    <body class="hold-transition login-page">
       
        <div class="register-box">
            <div class="register-logo">
                <a href="#"><b>Admin</b>LTE</a>
            </div>

            <div class="register-box-body">
                <h3>Welcome</h3> 
                <h3>Sign in or Sign up</h3>
                <div class="row">
                    <div class="col-sm-6">
                        <a class="btn btn-primary" href="home">Sing in</a>
                        
                    </div>
                    <div class="col-sm-6">
                        
                        <a class="btn btn-primary" href="register">Sing up</a>
                    </div>
                </div>

                
                
            </div>
            <!-- /.form-box -->
        </div>
        
        
    </body>
</html>
