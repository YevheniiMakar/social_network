<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>AdminLTE 2 | User Profile</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="static/bower_components/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="static/bower_components/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="static/bower_components/Ionicons/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="static/dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="static/dist/css/skins/_all-skins.min.css">
        <script src="static/plugins/jquery/jquery-3.2.1.min.js"></script>
        <script src="static/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="static/plugins/bootpag/jquery.bootpag.min.js"></script>
        <!--<script src="http://botmonster.com/jquery-bootpag/jquery.bootpag.js"></script>-->


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- add new post -->

        <style>

            .img-container-add-outer {
                position: relative;
                float: left;
                width: 150px;
                height: 150px;
                margin: 10px;
                border: 1px solid #CCC;
            }

            .img-container-add-inner {
                position: absolute;
                top: 5%;
                left: 5%;
                width: 130px;
                height: 130px;

                overflow: hidden;
            }
            .img-container-add-span {
                display: block;
                position: absolute;
                left: 50%;
                width: 500em;
                height: 100%;
                margin-left: -250em;
                text-align: center;
            }
            .img-content-add-img {
                height: 100%;
                vertical-align: bottom;
            }

        </style>


        <!-- add new post -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <!--<script src="https://code.jquery.com/jquery-3.2.1.min.js" type="text/javascript"></script>-->
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">

            <header class="main-header">
                <!-- Logo -->
                <a href="/home" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>A</b>LT</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>Admin</b>LTE</span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="/myfinalproject-web/file/${currentuser.photo}" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">

                            <a href="/myfinalproject-web/id${currentuser.id}"><p>${currentuser.firstname} ${currentuser.lastname}</p> </a>
                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu" data-widget="tree">
                        <li class="header">MAIN NAVIGATION</li>
                        <li>
                            <a href="/myfinalproject-web/id${currentuser.id}">
                                <i class="fa fa-files-o"></i>
                                <span>My page</span>
                            </a>
                        </li>
                        <li>
                            <a href="/myfinalproject-web/id${currentuser.id}/friends">
                                <i class="fa fa-files-o"></i>
                                <span>Friend</span>
                                <span class="pull-right-container">
                                    <span class="label label-primary pull-right">1112</span>
                                </span>
                            </a>
                        </li>
                        <!--                        <li class="treeview">
                                                    <a href="#">
                                                        <i class="fa fa-files-o"></i>
                                                        <span>Massage</span>
                                                        <span class="pull-right-container">
                                                            <span class="label label-primary pull-right">1122</span>
                                                        </span>
                                                    </a>
                                                </li>-->
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-dashboard"></i> <span>Posts</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="/myfinalproject-web/id${currentuser.id}/posts_post"><i class="fa fa-circle-o"></i> My posts</a></li>
                                <li><a href="/myfinalproject-web/id${currentuser.id}/posts_allnews"><i class="fa fa-circle-o"></i> News</a></li>

                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-dashboard"></i> <span>Users</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="/myfinalproject-web/id${currentuser.id}/userslist_friends"><i class="fa fa-circle-o"></i> Friends</a></li>
                                <li><a href="/myfinalproject-web/id${currentuser.id}/userslist_users"><i class="fa fa-circle-o"></i> Users</a></li>

                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-dashboard"></i> <span>Media</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="/myfinalproject-web/id${currentuser.id}/medias_images"><i class="fa fa-circle-o"></i> Images</a></li>
                                <li><a href="/myfinalproject-web/id${currentuser.id}/medias_audios"><i class="fa fa-circle-o"></i> Audio</a></li>
                                <li><a href="/myfinalproject-web/id${currentuser.id}/medias_videos"><i class="fa fa-circle-o"></i> Video</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="/myfinalproject-web/id${currentuser.id}/profile">
                                <i class="fa fa-files-o"></i>
                                <span>Profile</span>
                            </a>
                        </li>

                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <section class="content">

                    <div class="row">
                        <div class="col-md-3">

                            <!-- Profile Image -->
                            <div class="box box-primary" style="border-radius: 15px 15px 15px 15px">
                                <div class="box-body box-profile">
                                    <img class="profile-user-img img-responsive img-circle" src="/myfinalproject-web/file/${user.photo}" alt="User profile picture">

                                    <h3 class="profile-username text-center">${user.firstname} ${user.lastname}</h3>

                                    <ul class="list-group list-group-unbordered">
                                        <li class="list-group-item">
                                            <a href="/myfinalproject-web/id${user.id}/friends"><b>Friends</b></a>
                                        </li>
                                    </ul>
                                                
                                    <c:if test="${currentuser.id ne user.id}">
                                    <c:if test="${checkfriend ne 'friend'}">
                                        <a href="#" class="btn btn-primary btn-block ra" ><b>Add to Friends</b></a>
                                    </c:if>
                                    </c:if>
                                </div>

                            </div>
                            <!-- /.box -->

                            <!-- About Me Box -->

                            <!-- /.box -->
                        </div>
                        <!-- /.col -->
                        <div class="col-md-9">
                            <!-- /.box user cintent -->
                            <div class="box box-primary" style="border-radius: 15px 15px 15px 15px">
                                <div class="box-body">
                                    <!-- /.box user fotos-->
                                    <div class="box-header with-border" style="background-color: #3c8dbc; border-radius: 9px 9px 0px 0px">
                                        <h3 class="box-title"  >Photos</h3>

                                    </div>
                                    <div class="box-body">
                                        <div class="box-body margin-center">
                                            <c:forEach var="imagev" items="${imageview}" >
                                                <div class="col-sm-3">
                                                    <a href="" class="img-responsive">
                                                        <img class="img-responsive" style="border-radius: 15px" src="/myfinalproject-web/file/${imagev.name}" alt="Photos">
                                                    </a>
                                                </div>
                                            </c:forEach>
                                        </div>
                                    </div>

                                    <div class="box-body" style="background-color: #9dc6dd; border-radius: 0px 0px 9px 9px">
                                        <div class="pull-right">

                                            <a href="/myfinalproject-web/id${user.id}/medias_images" style="color: #fff">View all</a>
                                        </div>
                                    </div>

                                    <!-- /.box-body -->
                                </div>
                                <div class="box-body">

                                    <div class="box-header with-border" style=" background-color: #3c8dbc; border-radius: 9px 9px 0px 0px">
                                        <h3 class="box-title">Videos</h3>
                                    </div>
                                    <div class="box-body">
                                        <div class="box-body margin-center">
                                            <c:forEach var="videov" items="${videoview}">
                                                <div class="col-sm-3">
                                                    <a href="/myfinalproject-web/file/${videov.name}" class="img-responsive">
                                                        <img class="img-responsive" src="/myfinalproject-web/file/video.jpeg" alt="Photos">
                                                    </a>
                                                </div>
                                            </c:forEach>
                                        </div>
                                    </div>
                                    <div class="box-body" style="background-color: #9dc6dd; border-radius: 0px 0px 9px 9px">
                                        <div class="pull-right">

                                            <a href="/myfinalproject-web/id${user.id}/medias_videos" style="color: #fff">View all</a>
                                        </div>
                                    </div>



                                </div>


                                <div class="box-body">
                                    <div class="box-header with-border" style=" background-color: #3c8dbc; border-radius: 9px 9px 0px 0px">
                                        <h3 class="box-title">Audios</h3>
                                    </div>

                                    <div class="box-body">
                                        <table class="table table-condensed">
                                            <tbody>

                                                <tr>
                                                    <th>Artist - title</th>
                                                    <th>Progress</th>
                                                    <th style="width: 40px">like/add</th>
                                                </tr>
                                                <c:forEach var="audiov" items="${audioview}" >
                                                    <tr>

                                                        <td>${audiov.name}</td>
                                                        <td>
                                                            <audio controls>
                                                                <source src="/myfinalproject-web/file/${audiov.name}" type="audio/mpeg">
                                                                Тег audio не поддерживается вашим браузером. 
                                                                <a href="/myfinalproject-web/file/${audiov.name}">Скачайте музыку</a>.
                                                            </audio>
                                                        </td>
                                                        <td>ghghjgh</td>
                                                    </tr>
                                                </c:forEach>

                                            </tbody></table>

                                    </div>
                                    <div class="box-body" style="background-color: #9dc6dd ; border-radius: 0px 0px 9px 9px">
                                        <div class="pull-right">

                                            <a href="/myfinalproject-web/id${user.id}/medias_audios" style="color: #fff">View all</a>
                                        </div>
                                    </div>



                                </div>

                                <!-- /.box-body -->
                            </div>
                            <div class="box" style="border-color: #fff; border-radius: 15px" >
                                <div class="box-body">
                                    <div class="box-header with-border" style=" background-color: #3c8dbc; border-radius: 9px 9px 0px 0px">

                                        <h3 class="box-title">Wall</h3>

                                    </div>
                                </div>
                                <div class="box-body">

                                    <input id="addPost" class="form-control" maxlength="2000" type="text">
                                    <input type="button" value="addPost" onclick="doAddPost()">
                                    <!--загружаем медиа для поста-->
                                    <form id="upload-file-form">

                                        <input id="upload-file-input" type="file" name="uploadfile"  value="upload file" />
                                    </form>
                                    <div ><table class="table table-condensed"><tbody id="view-medias-file-audio"></tbody></table></div>
                                    <div id="view-medias-file-video"></div>
                                    <div id="view-medias-file-images"></div>


                                </div>
                                <div class="box-body">
                                    <div class="box-body">



                                        <div class="box-body">
                                            <c:forEach var="postl" items="${postlist}" >
                                                <div class="post">
                                                    <div class="user-block">
                                                        <img class="img-circle img-bordered-sm" src="/myfinalproject-web/file/${postl.user.photo}" alt="user image">
                                                        <span class="username">
                                                            <a href="#">${postl.user.firstname} ${postl.user.lastname}</a>
                                                            <a href="#" class="pull-right btn-box-tool"><i class="fa fa-times"></i></a>
                                                        </span>
                                                        <!--<span class="description">Shared publicly - 7:30 PM today</span>-->
                                                    </div>
                                                    <!-- /.user-block -->
                                                    <p>${postl.post}</p>




                                                    <ul class="list-inline">
                                                        <li><a href="#" class="link-black text-sm"><i class="fa fa-share margin-r-5"></i> Share</a></li>
                                                        <li><a href="#" class="link-black text-sm"><i class="fa fa-thumbs-o-up margin-r-5"></i> Like</a></li>    
                                                    </ul>
                                                    <table>
                                                        <c:forEach var="postma" items="${postl.mediasList}" >

                                                            <c:if test="${postma.mediaType eq 'AUDIO'}">
                                                                <tr>
                                                                    <td>${postm.name}</td>
                                                                    <td>
                                                                        <audio controls>
                                                                            <source src="/myfinalproject-web/file/${postma.name}" type="audio/mpeg">
                                                                            Тег audio не поддерживается вашим браузером. 
                                                                            <a href="/myfinalproject-web/file/${postma.name}">Скачайте музыку</a>.
                                                                        </audio>
                                                                    </td>

                                                                </tr>
                                                            </c:if>


                                                        </c:forEach>
                                                    </table>

                                                    <c:forEach var="postmp" items="${postl.mediasList}" >

                                                        <c:if test="${postmp.mediaType eq 'PICTURE'}">
                                                            <div class="col-sm-3">
                                                                <a href="" class="img-responsive">
                                                                    <img class="img-responsive" style="border-radius: 15px" src="/myfinalproject-web/file/${postmp.name}" alt="Photos">
                                                                </a>
                                                            </div>
                                                        </c:if>
                                                    </c:forEach>


                                                </div>
                                            </c:forEach>

                                        </div>


                                        <!-- /.col -->
                                    </div>
                                    <div id="page-selection"></div>

                                </div>
                            </div>
                            <!-- /.row -->

                            </section>
                            <!-- /.content -->
                        </div>
                        <!-- /.content-wrapper -->
                        <footer class="main-footer">
                            <div class="pull-right hidden-xs">
                                <b>Version</b> 2.4.0
                            </div>
                            <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
                            reserved.
                        </footer>

                        <!--                         Control Sidebar 
                                                <aside class="control-sidebar control-sidebar-dark">
                                                     Create the tabs 
                                                    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
                                                        <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
                                                        <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
                                                    </ul>
                                                     Tab panes 
                                                    <div class="tab-content">
                                                         Home tab content 
                                                        <div class="tab-pane" id="control-sidebar-home-tab">
                                                            <h3 class="control-sidebar-heading">Recent Activity</h3>
                                                            <ul class="control-sidebar-menu">
                                                                <li>
                                                                    <a href="javascript:void(0)">
                                                                        <i class="menu-icon fa fa-birthday-cake bg-red"></i>
                        
                                                                        <div class="menu-info">
                                                                            <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>
                        
                                                                            <p>Will be 23 on April 24th</p>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="javascript:void(0)">
                                                                        <i class="menu-icon fa fa-user bg-yellow"></i>
                        
                                                                        <div class="menu-info">
                                                                            <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>
                        
                                                                            <p>New phone +1(800)555-1234</p>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="javascript:void(0)">
                                                                        <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>
                        
                                                                        <div class="menu-info">
                                                                            <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>
                        
                                                                            <p>nora@example.com</p>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="javascript:void(0)">
                                                                        <i class="menu-icon fa fa-file-code-o bg-green"></i>
                        
                                                                        <div class="menu-info">
                                                                            <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>
                        
                                                                            <p>Execution time 5 seconds</p>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                             /.control-sidebar-menu 
                        
                                                            <h3 class="control-sidebar-heading">Tasks Progress</h3>
                                                            <ul class="control-sidebar-menu">
                                                                <li>
                                                                    <a href="javascript:void(0)">
                                                                        <h4 class="control-sidebar-subheading">
                                                                            Custom Template Design
                                                                            <span class="label label-danger pull-right">70%</span>
                                                                        </h4>
                        
                                                                        <div class="progress progress-xxs">
                                                                            <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="javascript:void(0)">
                                                                        <h4 class="control-sidebar-subheading">
                                                                            Update Resume
                                                                            <span class="label label-success pull-right">95%</span>
                                                                        </h4>
                        
                                                                        <div class="progress progress-xxs">
                                                                            <div class="progress-bar progress-bar-success" style="width: 95%"></div>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="javascript:void(0)">
                                                                        <h4 class="control-sidebar-subheading">
                                                                            Laravel Integration
                                                                            <span class="label label-warning pull-right">50%</span>
                                                                        </h4>
                        
                                                                        <div class="progress progress-xxs">
                                                                            <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="javascript:void(0)">
                                                                        <h4 class="control-sidebar-subheading">
                                                                            Back End Framework
                                                                            <span class="label label-primary pull-right">68%</span>
                                                                        </h4>
                        
                                                                        <div class="progress progress-xxs">
                                                                            <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                             /.control-sidebar-menu 
                        
                                                        </div>
                                                         /.tab-pane 
                                                         Stats tab content 
                                                        <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
                                                         /.tab-pane 
                                                         Settings tab content 
                                                        <div class="tab-pane" id="control-sidebar-settings-tab">
                                                            <form method="post">
                                                                <h3 class="control-sidebar-heading">General Settings</h3>
                        
                                                                <div class="form-group">
                                                                    <label class="control-sidebar-subheading">
                                                                        Report panel usage
                                                                        <input type="checkbox" class="pull-right" checked>
                                                                    </label>
                        
                                                                    <p>
                                                                        Some information about this general settings option
                                                                    </p>
                                                                </div>
                                                                 /.form-group 
                        
                                                                <div class="form-group">
                                                                    <label class="control-sidebar-subheading">
                                                                        Allow mail redirect
                                                                        <input type="checkbox" class="pull-right" checked>
                                                                    </label>
                        
                                                                    <p>
                                                                        Other sets of options are available
                                                                    </p>
                                                                </div>
                                                                 /.form-group 
                        
                                                                <div class="form-group">
                                                                    <label class="control-sidebar-subheading">
                                                                        Expose author name in posts
                                                                        <input type="checkbox" class="pull-right" checked>
                                                                    </label>
                        
                                                                    <p>
                                                                        Allow the user to show his name in blog posts
                                                                    </p>
                                                                </div>
                                                                 /.form-group 
                        
                                                                <h3 class="control-sidebar-heading">Chat Settings</h3>
                        
                                                                <div class="form-group">
                                                                    <label class="control-sidebar-subheading">
                                                                        Show me as online
                                                                        <input type="checkbox" class="pull-right" checked>
                                                                    </label>
                                                                </div>
                                                                 /.form-group 
                        
                                                                <div class="form-group">
                                                                    <label class="control-sidebar-subheading">
                                                                        Turn off notifications
                                                                        <input type="checkbox" class="pull-right">
                                                                    </label>
                                                                </div>
                                                                 /.form-group 
                        
                                                                <div class="form-group">
                                                                    <label class="control-sidebar-subheading">
                                                                        Delete chat history
                                                                        <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
                                                                    </label>
                                                                </div>
                                                                 /.form-group 
                                                            </form>
                                                        </div>
                                                         /.tab-pane 
                                                    </div>
                                                </aside>-->
                        <!-- /.control-sidebar -->
                        <!-- Add the sidebar's background. This div must be placed
                             immediately after the control sidebar -->
                        <div class="control-sidebar-bg"></div>
                    </div>
                    <!-- ./wrapper -->

                    <!-- jQuery 3 -->
                    <script>
                        // init bootpag
                        $('#page-selection').bootpag({

                            total: ${pagecount},
                            page:${numePage}
                        }).on("page", function (event, /* page number here */ num) {
                            $("#content").html("Insert content"); // some ajax content loading...
                        });
                    </script>
                    <!--загружаем файлы на сервер-->

                    <script>
                        var mediasAudio = new Array();//
                        var mediasVideo = new Array();//
                        var mediasImages = new Array();
                        var mediasAll = new Array();


                        $(document).ready(function () {
                            $("#upload-file-input").on("change", uploadFile);

                        });
                        function uploadFile() { // функция на загрузку файлов на серер
                            var length = mediasAll.length;
                            if (length <= 10) {

                                /* сам запрос на загрузку данных на сервер*/
                                $.ajax({
                                    url: '/myfinalproject-web/id${currentuser.id}/addmedias',
                                    type: 'POST',
                                    data: new FormData($("#upload-file-form")[0]),
                                    enctype: 'multipart/form-data',
                                    processData: false,
                                    contentType: false,
                                    cache: false,
                                    dataType: 'json',
                                    success: function (data) {
                                        alert(JSON.stringify(data.media));
                                        sortMedia(data.media);

                                        viewAudio();
                                        viewVideo();
                                        viewImages();

                                        $("#upload-file-message").text("File succesfully uploaded");
                                        $("#upload-file-message").text();
                                    },
                                    error: function () {
                                        // Handle upload error
                                        $("#upload-file-message").text(
                                                "File not uploaded (perhaps it's too much big)");
                                    }
                                });
                            }
                            ;
                        }
                        ;
                        function sortMedia(medias) {
                            if (medias.mediaType === 'PICTURE') {
                                mediasImages.push(medias);
                                mediasAll.push(medias);

                            }
                            if (medias.mediaType === 'VIDEO') {
                                mediasVideo.push(medias);
                                mediasAll.push(medias);
                            }
                            if (medias.mediaType === 'AUDIO') {
                                mediasAudio.push(medias);
                                mediasAll.push(medias);

                            }
                        }
                        ;
                        function viewAudio() {

                            var res = '';
                            for (var i = 0; i < mediasAudio.length; i++) {
                                var str = '<tr><td>' + mediasAudio[i].name + '</td><td><audio controls><source src="/myfinalproject-web/file/' + mediasAudio[i].name + '" type="audio/mpeg"></audio></td></tr>';
                                res = res + str;
                                //<img src="images/redcat.jpg" alt="Задана ширина" width="200">
                            }
                            $("#view-medias-file-audio").html(res);
                        }
                        ;
                        function viewVideo() {

                            var res = '';

                            for (var i = 0; i < mediasVideo.length; i++) {

                                var str = '<div class="img-container-add-outer"><div class="img-container-add-inner"><span class="img-container-add-span"><a href="/myfinalproject-web/file' + video.jpeg + '"><img class="img-content-add-img" src="/myfinalproject-web/file/' + mediasImages[i].name + '" alt="альтернативный текст"></span></div></div>';

                                res = res + str;
                            }
                            $("#view-medias-file").html(res);
                        }
                        ;
                        function viewImages() {


                            var res = '';

                            for (var i = 0; i < mediasImages.length; i++) {


                                var str = '<div class="img-container-add-outer"><div class="img-container-add-inner"><span class="img-container-add-span"><img class="img-content-add-img" src="/myfinalproject-web/file/' + mediasImages[i].name + '" alt="альтернативный текст"></span></div></div>';

                                res = res + str;
                                //"<img src= \"/myfinalproject-web/file/\""+mediasAudio[i].name+"\" width=\"200\">";

                                //<!--<img src="images/redcat.jpg" alt="Задана ширина" width="200">--> 
                            }
                            //$("#view-medias-file-video").html("<img src= \"/myfinalproject-web/file/"+mediasImages[0].name+"\" width=\"200\">");
                            $("#view-medias-file-images").html(res);
                        }
                        ;



                        function doAddPost() { // функция предачи текстового содержимого поста + информации я о файлах прикрепленных к посту

                            var text = $("#addPost").val();
                            var creatPost = {"text": text, "mediasObjects": mediasAll};
                            //mediasAll = Json.stri
                            /*функция передачи текста и файлов*/
                            $.ajax({
                                url: '/myfinalproject-web/id${currentuser.id}/addpost',
                                type: 'POST',
                                //dataType: 'json',
                                contentType: 'application/json',
                                data: JSON.stringify(creatPost),
                                success: function (data) {

                                    var result = data;
                                    $("#result_text").text(result);//не уверен что правильно передаю JSON на сервер
                                }
                            });
                        }
                        ;
                    </script>
                    <!--пагинация-->


                    <script src="static/bower_components/jquery/dist/jquery.min.js"></script>
                    <!-- Bootstrap 3.3.7 -->
                    <script src="static/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
                    <!-- FastClick -->
                    <script src="static/bower_components/fastclick/lib/fastclick.js"></script>
                    <!-- AdminLTE App -->
                    <script src="static/dist/js/adminlte.min.js"></script>
                    <!-- AdminLTE for demo purposes -->
                    <script src="static/dist/js/demo.js"></script>
                    <script src="static/bower_components/ckeditor/ckeditor.js"></script>
                    <!-- Bootstrap WYSIHTML5 -->
                    <script src="static/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>

                    </body>
                    </html>
